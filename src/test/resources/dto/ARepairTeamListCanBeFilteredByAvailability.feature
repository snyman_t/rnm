Feature: A RepairTeam list can be filtered

Scenario: Based on Team availability

Given a list of RepairTeams
When I apply an AVAILABILITY filter with filter value "01-06-2015"
Then the new filtered RepairTeam list only contains RepairTeams which are available at "01-06-2015"




	