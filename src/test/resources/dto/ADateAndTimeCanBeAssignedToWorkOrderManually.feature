Feature: A Date and Time can be assigned to a WorkOrder manually

Scenario: Add a calendar entry to a calendar

Given a calendar containing an entry with start date and time "2015-06-01 08:00" and duration of 4 hours 
When another entry with a start date and time "2015-06-01 11:59" and duration 4 hours is assigned to the calendar
Then a clash is reported
And the calendar still only contains the original 1 entry


Scenario: Add a calendar entry to a WorkOrder's assigned RepairTeam's calendar

Given a WorkOrder with state ASSIGNED 
When I assign CalendarEntry to the assigned RepairTeam's calendar
Then the WorkOrder status is SCHEDULED








	