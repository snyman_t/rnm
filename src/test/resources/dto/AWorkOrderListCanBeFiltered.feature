Feature: A WorkOrder list can be filtered

Scenario Outline: Based on fault type

	Given a list of WorkOrders for faults with faultType:
	
	|"POTHOLE"|
	|"DRAINAGE"|
	|"TRAFFICLIGHT"|

	
	
	When I apply a filter with a filter type FAULTTYPE and filter value <filterValue>
	
	Then the new filtered WorkOrder list only contains WorkOrders for faults with faultType <filterValue>
	
	Examples:
	
	|filterValue|
	|"POTHOLE"|
	|"DRAINAGE"|	
	
Scenario Outline: Based on location

	Given a list of WorkOrders for faults with addresses:
	
	|"21 Jump Street, Hatfield, Pretoria"|
	|"1 Eloff Street, Camps Bay, Cape Town"|
	|"15 Shaka Road, Kingsley, Durban"|

	When I apply a filter with filter type <filterType> and filter value <filterValue>
	
	Then the new filtered WorkOrder list only contains WorkOrders for faults with <filterType> as <filterValue>
	
	Examples:
	
	|filterType|filterValue|
	|"ADRESS.SUBURB"|"Hatfield"|
	|"ADRESS.CITY"|"Pretoria"|		