Feature: Fault Logging Scenario

This contains all the scenario tests related to Fault Logging

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 1-Log a Single Fault ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario tests a single fault, with specified type, description and street location
# will be logged into the Fault database, with the same values as specified, as well as that
# some attributes will automatically be assigned values (ID='1', priority=NORMAL and source='CALL_CENTRE')
# -----------------------------------------------------------------------------------------------------------
     Scenario: 1-Log a Single Fault
     Given A Fault exists with type='POTHOLE'; description='ABC'; StreetName='Empire'; StreetSuburb='Braamfontein'; xCoordinate='100'; yCoordinate='200'
     And a Member calls to report the fault
     When Call-Centre Agent logs the fault
     And the new fault is logged with priority='NORMAL', status='NEW'
     Then Fault database size should be 1
     And fault database with ID='1' must have values type='POTHOLE'; description='ABC'; StreetName='Empire'; Suburb='Braamfontein'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='100'; yCoordinate='200'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 2-Log a Single Fault with Call ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario logs a single fault as per Scenario 1,
# however also tests log into Call database, and returning of a Call Reference
# -----------------------------------------------------------------------------------------------------------
     Scenario: 2-Log a Single Fault with Call
     Given A Fault exists with type='POTHOLE'; description='ABC'; StreetName='Empire'; StreetSuburb='Braamfontein'; xCoordinate='100'; yCoordinate='200'
     And a Member calls to report the fault
     When Call-Centre Agent logs the fault
     And the new fault is logged with priority='NORMAL', status='NEW'
     Then Fault database size should be 1
     And Call database size should be 1
     And Call database with ID='1' must have values callReferenceID='REF0000000001'; callState='NEW'; faultLinkage='1'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 3-Log Multiple Faults ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario tests setup of multiple faults within the database
# -----------------------------------------------------------------------------------------------------------
    Scenario: 3-Log Multiple Faults
    Given The fault database has multiple entries:
    |FaultID  |FaultType    |Description|StreetName    |StreetSuburb |Pririty|Status| xCoordinate | yCoordinate |
    |1        |POTHOLE      |SomeFaultA |Empire Street |Braamfontein |NORMAL |NEW   | 100         | 200         |
    |2        |DRAINAGE     |SomeFaultB |West Street   |Sandton      |NORMAL |NEW   | 300         | 200         |
    |3        |TRAFFICLIGHT |SomeFaultC |AliceLane     |Sandton      |NORMAL |NEW   | 300         | 250         |
    When faults are logged by Call Centre Operator
    Then Fault database size should be 3
    And fault database with ID='1' must have values type='POTHOLE'; description='SomeFaultA'; StreetName='Empire Street'; Suburb='Braamfontein'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='100'; yCoordinate='200'
    And fault database with ID='2' must have values type='DRAINAGE'; description='SomeFaultB'; StreetName='West Street'; Suburb='Sandton'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='300'; yCoordinate='200'
    And fault database with ID='3' must have values type='TRAFFICLIGHT'; description='SomeFaultC'; StreetName='AliceLane'; Suburb='Sandton'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='300'; yCoordinate='250'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 4-Query by FaultID ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario queries a fault by ID
# -----------------------------------------------------------------------------------------------------------
    Scenario: 4-Query by FaultID
    Given The fault database has multiple entries:
    |FaultID  |FaultType    |Description|StreetName    |StreetSuburb |Pririty|Status| xCoordinate | yCoordinate |
    |1        |POTHOLE      |SomeFaultA |Empire Street |Braamfontein |NORMAL |NEW   | 100         | 200         |
    |2        |DRAINAGE     |SomeFaultB |West Street   |Sandton      |NORMAL |NEW   | 300         | 200         |
    |3        |TRAFFICLIGHT |SomeFaultC |AliceLane     |Sandton      |NORMAL |NEW   | 300         | 250         |
    When there is a call query for FaultID='1'
    Then the fault found has values ID='1'; type='POTHOLE'; description='SomeFaultA'; StreetName='Empire Street'; Suburb='Braamfontein'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='100'; yCoordinate='200'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 5-Query by GeoLocation ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario queries a fault by fault street address
# -----------------------------------------------------------------------------------------------------------
    Scenario: 5-Query by GeoLocation
    Given The fault database has multiple entries:
    |FaultID  |FaultType    |Description|StreetName    |StreetSuburb |Pririty|Status| xCoordinate | yCoordinate |
    |1        |POTHOLE      |SomeFaultA |Empire Street |Braamfontein |NORMAL |NEW   | 100         | 200         |
    |2        |DRAINAGE     |SomeFaultB |West Street   |Sandton      |NORMAL |NEW   | 300         | 200         |
    |3        |TRAFFICLIGHT |SomeFaultC |AliceLane     |Sandton      |NORMAL |NEW   | 300         | 250         |
    When there is a call query for Geocoded XCoOrdinate='300'; YCoOrdinate='250'
    Then the fault found has values ID='3'; type='TRAFFICLIGHT'; description='SomeFaultC'; StreetName='AliceLane'; Suburb='Sandton'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='300'; yCoordinate='250'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 6- Derive geolocation for given street address ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario derives a GeoLocation for a given street address
# NOTE : this assumes avaliability of a comprehensive "geolocation" database - mapping street address to geolocation
# sample database records are initialized for testing purpose
# -----------------------------------------------------------------------------------------------------------
    Scenario: 6- Derive GeoLocation for given street address
    Given The geolocation database has multiple entries:
    |Empire Street |Braamfontein |100 |200 |
    |West Street   |Sandton      |300 |200 |
    |AliceLane     |Sandton      |300 |250 |
    |Fredman Drive |Sandton      |350 |300 |
    When search input is StreetName='West Street' and Suburb='Sandton'
    Then search result by Street Address is XCoordinate='300' and YCoordinate='200'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 7- Derive geolocation for given street address ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario derives a street address for a given GeoLocation
# NOTE : this assumes avaliability of a comprehensive "geolocation" database - mapping street address to geolocation
# sample database records are initialized for testing purpose
# -----------------------------------------------------------------------------------------------------------
    Scenario: 7- Derive street address for given GeoLocation
    Given The geolocation database has multiple entries:
    |Empire Street |Braamfontein |100 |200 |
    |West Street   |Sandton      |300 |200 |
    |AliceLane     |Sandton      |300 |250 |
    |Fredman Drive |Sandton      |350 |300 |
    When search input is XCoordinate='350' and YCoordinate='300'
    Then search result by Street GeoLocation is StreetName='Fredman Drive' and Suburb='Sandton'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario Outline: 8- Get Nearby Faults to given street address ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario finds nearby faults given street address
# NOTE : this implements a basic match on both StreetName and Suburb
# Integration into a production system Geolocation database will offer more advanced search
# -----------------------------------------------------------------------------------------------------------
    Scenario Outline: 7- Get Nearby Faults to given street address
    Given The fault database has multiple entries:
    |FaultID  |FaultType    |Description|StreetName    |StreetSuburb |Pririty|Status|xCoordinate | yCoordinate |
    |1        |POTHOLE      |SomeFaultA |Empire Street |Braamfontein |NORMAL |NEW   |100         | 200         |
    |2        |DRAINAGE     |SomeFaultB |West Street   |Braamfontein |NORMAL |NEW   |600         | 650         |
    |3        |TRAFFICLIGHT |SomeFaultC |West Street   |Sandton      |NORMAL |NEW   |300         | 200         |
    |4        |POTHOLE      |SomeFaultD |West Street   |Sandton      |NORMAL |NEW   |300         | 200         |
    |5        |DRAINAGE     |SomeFaultE |AliceLane     |Sandton      |NORMAL |NEW   |300         | 250         |
    |6        |POTHOLE      |SomeFaultF |Fredman       |Sandton      |NORMAL |NEW   |300         | 300         |
    When search input is StreetName=<filterStreetName> and Suburb=<filterSuburbName>
    Then NearbyFaults searched by street address is <ExpectedResultSize>
    And NearbyFaultList contains FaultID=<ExpectedFaultID_A>
    And NearbyFaultList contains FaultID=<ExpectedFaultID_B>

	Examples:
	|filterStreetName|filterSuburbName|ExpectedResultSize|ExpectedFaultID_A|ExpectedFaultID_B|
	|'West Street'   |'Sandton'       |2                 |'3'               |'4'             |
	|'West Street'   |'Braamfontein'  |1                 |'2'               |'2'             |
	|'Fredman'       |'Sandton'       |1                 |'6'               |'6'             |

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario Outline: 8- Get Nearby Faults to given GeoLocation ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario finds nearby faults given geolocation
# NOTE : this implements a basic distance calculation of the X and Y coordinates
# Integration into a production system Geolocation database will offer more advanced search
# -----------------------------------------------------------------------------------------------------------
    Scenario Outline: 8- Get Nearby Faults to given street address
    Given The fault database has multiple entries:
    |FaultID  |FaultType    |Description|StreetName    |StreetSuburb |Pririty|Status|xCoordinate | yCoordinate |
    |1        |POTHOLE      |SomeFaultA |Empire Street |Braamfontein |NORMAL |NEW   |600         | 600         |
    |2        |DRAINAGE     |SomeFaultB |West Street   |Braamfontein |NORMAL |NEW   |600         | 650         |
    |3        |TRAFFICLIGHT |SomeFaultC |West Street   |Sandton      |NORMAL |NEW   |300         | 200         |
    |4        |DRAINAGE     |SomeFaultE |AliceLane     |Sandton      |NORMAL |NEW   |300         | 250         |
    |5        |POTHOLE      |SomeFaultF |Fredman       |Sandton      |NORMAL |NEW   |300         | 300         |
    When search input is XCoordinate=<filterXCoordinate> and YCoordinate=<filterYCoordinate>
    Then NearbyFaults searched by GeoLocation <ExpectedResultSize>
    And NearbyFaultList contains FaultID=<ExpectedFaultID_A>
    And NearbyFaultList contains FaultID=<ExpectedFaultID_B>

	Examples:
	|filterXCoordinate|filterYCoordinate|ExpectedResultSize|ExpectedFaultID_A|ExpectedFaultID_B|
	|'300'            |'200'            |2                 |'3'               |'4'             |
	|'600'            |'600'            |2                 |'1'               |'2'             |
	|'300'            |'300'            |2                 |'4'               |'5'             |


# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 9-Fault Inspector can log Fault ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario tests that the Fault logged with only the Street address,
# can later have its GeoLocation accurately updated by the Fault Investigator
# -----------------------------------------------------------------------------------------------------------
    Scenario: 9-Fault Inspector can log Fault
     Given A Fault exists with type='POTHOLE'; description='ABC'; StreetName='Empire'; StreetSuburb='Braamfontein'; xCoordinate='100'; yCoordinate='200'
     When Fault inspector Mobile App logs the fault
     And the new fault is logged with priority='NORMAL', status='NEW'
     Then Fault database size should be 1
     And fault database with ID='1' must have values type='POTHOLE'; description='ABC'; StreetName='Empire'; Suburb='Braamfontein'; priority='NORMAL'; status='NEW'; source='MOBILE_APP'; xCoordinate='100'; yCoordinate='200'

# -----------------------------------------------------------------------------------------------------------
# ---------- Scenario: 10-Fault Inspector can Update a Fault Location ----------
# -----------------------------------------------------------------------------------------------------------
# This scenario tests that the Fault logged with only the Street address,
# can later have its GeoLocation accurately updated by the Fault Investigator
# -----------------------------------------------------------------------------------------------------------
    Scenario: 10-Fault Inspector can Update a Fault Location
    Given The fault database has multiple entries:
    |FaultID  |FaultType    |Description|StreetName    |StreetSuburb |Pririty|Status|xCoordinate | yCoordinate |
    |1        |POTHOLE      |SomeFaultA |Empire Street |Braamfontein |NORMAL |NEW   |0           | 0           |
    |2        |DRAINAGE     |SomeFaultB |West Street   |Sandton      |NORMAL |NEW   |300         | 200         |
    |3        |TRAFFICLIGHT |SomeFaultC |AliceLane     |Sandton      |NORMAL |NEW   |300         | 250         |
    When faults are logged by Fault Investigator
    And fault inspector updates coordinates of fault with ID='1' to have xCoordinate='888'; yCoordinate='999'
    Then Fault database size should be 3
    And fault database with ID='1' must have values type='POTHOLE'; description='SomeFaultA'; StreetName='Empire Street'; Suburb='Braamfontein'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='888'; yCoordinate='999'
    And fault database with ID='2' must have values type='DRAINAGE'; description='SomeFaultB'; StreetName='West Street'; Suburb='Sandton'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='300'; yCoordinate='200'
    And fault database with ID='3' must have values type='TRAFFICLIGHT'; description='SomeFaultC'; StreetName='AliceLane'; Suburb='Sandton'; priority='NORMAL'; status='NEW'; source='CALL_CENTRE'; xCoordinate='300'; yCoordinate='250'







