Feature: Work Order Fulfillment

Scenario: Team Leader needs mark off a task

Given a user attempts to view outstanding workorder tasks
When a task is completed
Then it can be marked off on the tasklist

Scenario: Team Leader needs to provide a reason for outstanding task

Given a user attempts to view outstanding workorder tasks
When a task is incomplete
Then it is marked incomplete on the tasklist and a reason is specified 

Scenario: Team Leader needs to complete workOrder

Given a user attempts to view outstanding workorder tasks
When a task list is complete
Then the workorder is marked as complete and the fault is marked as complete