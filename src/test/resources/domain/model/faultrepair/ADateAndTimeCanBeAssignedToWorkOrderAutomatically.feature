Feature: A Date and Time can be assigned to a WorkOrder automatically

Scenario: Using a domain service to assign a date and time to a work order automatically

Given a work order domain service for a domain containing a work order issued for a "low" priority fault 
And the work order has a repair team assigned to it 
And the repair team's calendar has multiple other entries:

#Fault Id |Fault Type|Fault Priority |Scheduled Date Time |Duration|
|1        |pothole   |low            |2015-06-01 08:00    |4       |
|2        |pothole   |low            |2015-06-01 12:00    |2       |
|3        |pothole   |low            |2015-06-01 14:00    |4       |

And the current date and time is "2015-06-01 08:00"

 
When the domain service requests to schedule the work order automatically for a duration of 4 hours


Then the work order domain service reports the work order status as "SCHEDULED"
And the work order domain service reports the scheduled date and time for the work order as "2015-06-01 18:00"



	