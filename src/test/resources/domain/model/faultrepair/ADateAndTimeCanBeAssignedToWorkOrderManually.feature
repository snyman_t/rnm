Feature: A Date and Time can be assigned to a WorkOrder manually

Scenario Outline: Adding a calendar entry to a calendar

Given a calendar containing an entry with start date and time <existingStartDateTime> and duration of <existingDuration>
When another entry with a start date and time <newStartDateTime> and duration <newDuration> is added to the calendar
Then the calendar reports that the operation was unsuccessful by returning <result>
And the calendar still only contains <entries> entries

Examples:

|existingStartDateTime|existingDuration|newStartDateTime  |newDuration|result|entries|
|"2015-06-01 08:00"   |4               |"2015-06-01 07:00"|4          |"false" |1      |  
|"2015-06-01 08:00"   |4               |"2015-06-01 12:00"|4          |"true"  |2      | 


Scenario: Using a domain service to schedule a repair team to complete a certain work order

Given a work order domain service for a domain containing a work order for fault id 1
And a repair team named "Team A" is assigned to the work order

When the work order gets scheduled at start date time "2015-06-01 08:00" and for a duration of 4 hours

Then the work order domain service reports the work order as "SCHEDULED"
And the work order domain service reports the work order scheduled date as "2015-06-01 08:00" and for a duration of 4 hours 






	