Feature: WorkOrder creation for verified Fault

Scenario: Basic WorkOrder Creation for fault needing repair

Given a verified Fault
When no WorkOrder exists for fault
Then create a new WorkOrder

Scenario: WorkOrder created with textual Tasks

Given a new WorkOrder is created
When a textual task description And no image is exists for fault
Then create a list of repair tasks

Scenario: WorkOrder created with textual Tasks and image

Given a new annotated Task is created
When a textual task description is given And an AnnotatedImage is provided for fault
Then create a list of RepairTasks with annotated Images

Scenario: WorkOrder Equipment and Materials Requirement

Given a new WorkOrder is successfully created
When a list of tasks is generated
Then associate a BillOfMaterials and a BillOfEquipment

Scenario: WorkOrder created by authorised staff member

Given a new  WorkOrder is to be created
When the user has permission to create WorkOrders
Then display the Option to create a WorkOrder