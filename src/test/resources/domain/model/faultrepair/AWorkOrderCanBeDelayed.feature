Feature: A WorkOrder can be delayed

Scenario: Delaying all work orders assigned to a repair team by delaying the first one

Given a domain service for a domain containing a list of work orders all assigned to one repair team:

#Fault Id |Fault Type|Fault Priority |Scheduled Date Time |Duration|
|1        |pothole   |low            |2015-06-01 08:00    |4       |
|2        |pothole   |low            |2015-06-01 12:00    |2       |
|3        |pothole   |low            |2015-06-01 14:00    |4       |

When I use the domain service to delay the work order assigned to fault id 1 by 2 hours

Then the domain service reports the below new dates and times for the work orders

#Fault Id |Fault Type|Fault Priority |Scheduled Date Time |Duration|
|1        |pothole   |low            |2015-06-01 10:00    |4       |
|2        |pothole   |low            |2015-06-01 14:00    |2       |
|3        |pothole   |low            |2015-06-01 16:00    |4       |





	