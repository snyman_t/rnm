Feature: A repair team can be assigned to a work order

Scenario: Using the domain service to assign a repair team to a work order

Given a work order domain service for a domain containing a work order
When I use the domain service to assign a repair team with id 34 to the work order
Then the domain service reports that the work order has a status of "ASSIGNED"
Then the domain service reports an assigned repair team id of 34 for the work order






