Feature: A WorkOrder list can be filtered

Scenario Outline: Based on fault type

	Given a domain service for a domain containing a list of work orders for the following faults with differing types:
	
	|1  |pothole       |high   |
	|2  |drainage      |low   |
	|3  |pothole       |low   |
	|4  |trafficlight  |low   |


	When I request to see only work orders with fault type <faultTypeFilter>
	
	Then the fault type of the associated fault for each work order returned should be <faultTypeFilter>
	
	Examples:
	|faultTypeFilter|
	|"pothole"|
	|"drainage"|
	|"trafficlight"|		
	
Scenario Outline: Based on priority

	Given a domain service for a domain containing a list of work orders for the following faults with differing priorities:
	
	|1  |pothole  |low   |
	|2  |pothole  |high  |
	|3  |pothole  |low   |
	|3  |pothole  |high  |


	When I request to see only work orders with priority <priorityFilter>
	
	Then the priority of the associated fault for each work order should be <priorityFilter>
	
	Examples:
	|priorityFilter|
	|"high"          |
	|"low"           |	