Feature: Work Order Fault Review Rules

Scenario: Authorised user can view Faults

Given a user attempts to view Faults
When the user has WorkOrder creation rights And chooses to view faults as a list
Then display the faults information as a List and display the fault priority

Scenario:  Authorised user can view Faults on Map

Given a user attempts to view Faults
When the user has WorkOrder creation rights And chooses to view faults on a map
Then display the faults information on a Map And display the fault priority

Scenario: Authorised user can drill down into report card

Given an authorised user is viewing fault
When they select the report card
Then they are able to see fault reportcard information

Scenario: Attach work order for verified and unassigned fault

Given a fault has no outstanding WorkOrders
When there are images on the original fault card 
Then complete workorder creation And set status to toschedule

Scenario: Faults are not modifiable if they are currently being modified by another user

Given a Fault needs to be modified
When it is already being modified
Then do not allow changes to the fault

Scenario: Fault with a verified workorder can receive new workorders

Given a fault has a WorkOrder assigned
When the WorkOrder has been verified
Then a new WorkOrder can be assigned