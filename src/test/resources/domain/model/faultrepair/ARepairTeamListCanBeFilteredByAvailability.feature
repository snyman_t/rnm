Feature: A RepairTeam list can be filtered

Scenario: Based on availability

	Given a domain service for a domain containing a list of repair teams each with a calendar entry :
	
    #Repair team name|Work order ID|Start date time  |Duration|  
	|Team A  		 |1            |2015-06-01 08:00 |4       |
	|Team B          |2            |2015-06-01 10:00 |2       |
	|Team C          |3            |2015-06-02 08:00 |4       |
	|Team D          |4            |2015-06-01 08:00 |4       |  


	When I request to see only repair teams that are available at "2015-06-01 08:00" for 4 hours 
	
	Then domain service should return only "Team C"
	

	