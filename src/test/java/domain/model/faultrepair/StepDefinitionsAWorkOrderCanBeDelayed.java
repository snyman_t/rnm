package domain.model.faultrepair;



import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.RepairTeamService;
import domain.service.faultrepair.WorkOrderService;

public class StepDefinitionsAWorkOrderCanBeDelayed {
	
	WorkOrderService workOrderService;
	RepairTeamService repairTeamService;
	int repairTeamId;
	int workOrderId;
	String currentDateTime;

	
	@Given("^a domain service for a domain containing a list of work orders all assigned to one repair team:$")
	public void a_domain_service_for_a_domain_containing_a_list_of_work_orders_all_assigned_to_one_repair_team(DataTable listOfWorkOrders)
																															throws Throwable { 
		workOrderService = new WorkOrderService();
		repairTeamService = new RepairTeamService(RepositoryFactory.getFactoryInstance().getRepairTeamRepository());
		
	    workOrderService.injectRepairTeamServiceDependancy(repairTeamService);
		repairTeamService.injectWorkOrderServiceDependancy(workOrderService);
	    
		repairTeamService.addARepairTeam("Team A");
		repairTeamId = repairTeamService.getListOfAllRepairTeamIds().get(0);
		
		List<List<String>> entries = listOfWorkOrders.asLists(String.class);
		
		for(List<String> row : entries){
				
			int faultId = Integer.parseInt(row.get(0));
			String faultType = row.get(1);
			String faultPriority = row.get(2);
			String startDateTime = row.get(3);
			int duration = Integer.parseInt(row.get(4));
			
			workOrderService.generateWorkOrderForFaultIDAndTypeAsString(faultId, faultType, faultPriority);
			int workOrderId = workOrderService.getWorkOrderIDForFaultID(faultId);
			
			workOrderService.assignRepairTeamToWorkOrder(workOrderId, repairTeamId);
			
		    workOrderService.scheduleDateTimeForAssignedWorkOrder(workOrderId, startDateTime, duration);
		}																													

	}

	@When("^I use the domain service to delay the work order assigned to fault id (\\d+) by (\\d+) hours$")
	public void i_use_the_domain_service_to_delay_the_work_order_assigned_to_fault_id_by_hours(int faultId, int delayHourCount) 
																													throws Throwable {
		workOrderId = workOrderService.getWorkOrderIDForFaultID(faultId);	
		workOrderService.delayWorkOrderByHours(workOrderId, delayHourCount);				
	}

	@Then("^the domain service reports the below new dates and times for the work orders$")
	public void the_domain_service_reports_the_below_new_dates_and_times_for_the_work_orders(DataTable listOfWorkOrders) 
																													throws Throwable {
		List<List<String>> entries = listOfWorkOrders.asLists(String.class);
		
		for(List<String> row : entries){
			
			int faultId = Integer.parseInt(row.get(0));
			String faultType = row.get(1);
			String faultPriority = row.get(2);
			String startDateTime = row.get(3);
			int duration = Integer.parseInt(row.get(4));
			
			int workOrderId = workOrderService.getWorkOrderIDForFaultID(faultId);
			
			Assert.assertEquals(startDateTime,repairTeamService.getScheduledDateTimeForWorkOrder(workOrderId));

		}
	}

}
