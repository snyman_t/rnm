package domain.model.faultrepair;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.RepairTeamService;

public class StepDefinitionsARepairTeamCanBeFilteredByAvailability {
	
	RepairTeamService repairTeamService;
	List<Integer> repairTeamIdList;
	
	@Given("^a domain service for a domain containing a list of repair teams each with a calendar entry :$")
	public void a_domain_service_for_a_domain_containing_a_list_of_repair_teams_each_with_a_calendar_entry(DataTable listOfRepairTeams) throws Throwable {
			
		repairTeamService = new RepairTeamService(RepositoryFactory.getFactoryInstance().getRepairTeamRepository());
		
		List<List<String>> entries = listOfRepairTeams.asLists(String.class);
		
		for(List<String> row : entries){
			
			String teamName = row.get(0);
			int workOrderId = Integer.parseInt(row.get(1));
			String startDateTime = row.get(2);
			int duration = Integer.parseInt(row.get(3));
			
			repairTeamService.addARepairTeam(teamName);
			
			int repairTeamId = repairTeamService.getRepairTeamId(teamName);
			
			repairTeamService.scheduleDateTimeForRepairTeamId(repairTeamId, workOrderId, startDateTime, duration);
		
		}
	}

	@When("^I request to see only repair teams that are available at \"(.*?)\" for (\\d+) hours$")
	public void i_request_to_see_only_repair_teams_that_are_available_at_for_hours(String startDateTime, int duration) throws Throwable {

			
		repairTeamIdList = repairTeamService.getRepairTeamIdsAvailableAtDateTime(startDateTime, duration);
	    
			
	}

	@Then("^domain service should return only \"(.*?)\"$")
	public void domain_service_should_return_only(String teamName) throws Throwable {
	    
	    Assert.assertEquals(1,repairTeamIdList.size());
	    Assert.assertEquals(teamName,repairTeamService.getRepairTeamName(repairTeamIdList.get(0)));
	    
	    
	}
	

}
