package domain.model.faultrepair.unittests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import domain.model.faultlogging.PRIORITY;
import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultrepair.Fault;
import domain.model.faultrepair.WorkOrder;
import domain.service.faultrepair.WorkOrderService;

public class UnitWorkOrderServiceTest {
	//private members here
	WorkOrderService _workOrderService;
	WorkOrder _workOrderForFault12,_workOrderForFault14,_workOrderForFault22;
	Fault _verifiedFaultInvalidIDPothole,_verifiedFault12Pothole,_verifiedFault14TrafficLight,_verifiedFault22Drainage;
	int _workOrderId,_workOrder2Id,_workOrder3Id;
	
	@Before
	public void upFront(){
		//fixtures
		_workOrderService = new WorkOrderService();
		_verifiedFaultInvalidIDPothole = new Fault(999,FAULTTYPE.POTHOLE);
		_verifiedFault12Pothole = new Fault(12,FAULTTYPE.POTHOLE);
		_verifiedFault14TrafficLight = new Fault(14,FAULTTYPE.TRAFFICLIGHT);
		_verifiedFault22Drainage = new Fault(22,FAULTTYPE.DRAINAGE);
		
		_workOrderForFault12 = _workOrderService.generateWorkOrderForFault(_verifiedFault12Pothole);
		_workOrderForFault14 = _workOrderService.generateWorkOrderForFault(_verifiedFault14TrafficLight);
		_workOrderForFault22 = _workOrderService.generateWorkOrderForFault(_verifiedFault22Drainage);
	}
	
	@Test
	public void testCanGenerateAWorkOrderWithIdAndFaultAsStringStringForApplicationServiceLevel(){
		int faultid=42;
		String faultTypeAsString = "Pothole";
		int workOrderId;
		int queriedWorkOrderId;
		String faultPriorityAsString = "low";
		
		workOrderId = _workOrderService.generateWorkOrderForFaultIDAndTypeAsString(faultid,faultTypeAsString,faultPriorityAsString);
		//confirm that the Id is returned for new work order, will be 1 as adding to empty repo
		queriedWorkOrderId = _workOrderService.getWorkOrderIDForFaultID(faultid);
		Assert.assertTrue(workOrderId==queriedWorkOrderId);
		
	}
	
	@Test//Steven
	public void testNoResultForEmptyRepository(){
		_workOrderId = _workOrderService.getWorkOrderIDForFaultID(_verifiedFaultInvalidIDPothole.getId());
		Assert.assertTrue(_workOrderId == 0);
	}
	
	@Test
	public void testCorrectResultForWorkOrderInRepository(){
		_workOrderId = _workOrderService.getWorkOrderIDForFaultID(_verifiedFault12Pothole.getId());
		Assert.assertTrue(_workOrderId == 1);
	}
	
	@Test
	public void testCorrectIDForWorkMultipleWorkOrdersAddedInRepository(){
		_workOrderId = _workOrderService.getWorkOrderIDForFaultID(_verifiedFault12Pothole.getId());
		_workOrder2Id = _workOrderService.getWorkOrderIDForFaultID(_verifiedFault14TrafficLight.getId());
		_workOrder3Id = _workOrderService.getWorkOrderIDForFaultID(_verifiedFault22Drainage.getId());
		
		Assert.assertTrue(_workOrderId == 1);
		Assert.assertTrue(_workOrder2Id == 2);
		Assert.assertTrue(_workOrder3Id == 3);
	}
}
