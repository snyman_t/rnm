package domain.model.faultrepair.unittests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultrepair.Fault;
import domain.model.faultrepair.WorkOrder;
import domain.service.faultrepair.WorkOrderServiceProxy;
import domain.service.faultlogging.IProfile.PROFILE_ROLE;
import domain.service.faultlogging.IProfile;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.SecurityException;

public class UnitWorkOrderServiceProxyTest {
	//private members here
	WorkOrderServiceProxy _workOrderServiceProxy;
	WorkOrder _workOrderForFault12,_workOrderForFault14,_workOrderForFault22;
	Fault _verifiedFaultInvalidIDPothole,_verifiedFault12Pothole,_verifiedFault14TrafficLight,_verifiedFault22Drainage;
	int _workOrderId,_workOrder2Id,_workOrder3Id;
	
	SecurityException e;
	
	@Test
	public void testCanAuthenticateUser(){
		
		try{
			Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
			_workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		} catch (SecurityException e){
			System.err.println("Caught IOException: " + e.getMessage());
		}
			
		Assert.assertEquals(null,e);
	}
	
	@Test
	public void testCanDenyAuthenticateUserOnInvalidCredentials(){
		
		try{
			Profile profile = new Profile(10, "workOrderCreator", "password", PROFILE_ROLE.FAULT_INVESTIGATOR);
			_workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		} catch (SecurityException e){
			System.err.println("Caught IOException: " + e.getMessage());
		}
		
	}
	
	@Test
	public void testAuthorisedUserCanUseService(){
		
		try{
			Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
			_workOrderServiceProxy = new WorkOrderServiceProxy(profile);
			int workOrderId = _workOrderServiceProxy.generateWorkOrderForFaultIDAndTypeAsString(111,"pothole","low");
			Assert.assertTrue(workOrderId==1);
		} catch (SecurityException e){
			System.err.println("Caught IOException: " + e.getMessage());
		}
			
		Assert.assertEquals(null,e);
	}
	
	@Test
	public void testUnAuthorisedUserCannotUseService(){
	//if this test fails an exception has been throw 
		
		try{
			Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.FAULT_INVESTIGATOR);
			_workOrderServiceProxy = new WorkOrderServiceProxy(profile);
			int workOrderId = _workOrderServiceProxy.generateWorkOrderForFaultIDAndTypeAsString(111,"pothole","low");
		} catch (SecurityException e){
			System.err.println("Caught IOException: " + e.getMessage());
		}
	}
}
