package domain.model.faultrepair;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;






import application.service.FaultLoggingService;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.WorkOrderService;



public class StepDefinitionsAssignRepairTeamToWorkOrder {
	
	private WorkOrder wo;
	private RepairTeam rt;
	private List<WorkOrder> woList;
	private List<WorkOrder> filteredWoList;

	private WorkOrderService domainService;
	private int workOrderId; 

	//Scenario: Using the domain service to assign a repair team to a work order
	
	@Given("^a work order domain service for a domain containing a work order$")
	public void a_work_order_domain_service_for_a_domain_containing_a_work_order() throws Throwable {
		domainService = new WorkOrderService();
        domainService.generateWorkOrderForFaultIDAndTypeAsString(1,"pothole","low");	
	    workOrderId = domainService.getWorkOrderIDForFaultID(1);
	}

	@When("^I use the domain service to assign a repair team with id (\\d+) to the work order$")
	public void i_use_the_domain_service_to_assign_a_repair_team_with_id_to_the_work_order(int repairTeamId) throws Throwable {
		domainService.assignRepairTeamToWorkOrder(workOrderId,repairTeamId);
	}


	@Then("^the domain service reports for the work order has a status of \"(.*?)\"$")
	public void the_domain_service_reports_for_the_work_order_has_a_status_of(String status) throws Throwable {
		Assert.assertEquals(status,domainService.getWorkOrderStatus(workOrderId));
	}

	@Then("^the domain service reports for the work order returns a assigned repair team id of (\\d+)$")
	public void the_domain_service_reports_for_the_work_order_returns_a_assigned_repair_team_id_of(int repairTeamId) throws Throwable {
		 Assert.assertEquals(repairTeamId, domainService.getAssignedRepairTeamIdForWorkOrder(workOrderId)); 
	}

}
