package domain.model.faultrepair;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.factory.faultrepair.RepairTeamFactory;
import domain.factory.faultrepair.WorkOrderFactory;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.RepairTeamService;
import domain.service.faultrepair.WorkOrderService;


public class StepDefinitionsADateAndTimeCanBeAssignedToWorkOrderManually {

	Calendar cal;
	Boolean response;
	
	WorkOrder wo;
	RepairTeam rt;
	
	private WorkOrderService workOrderService;
	private RepairTeamService repairTeamService;
	private int workOrderId;
	private int repairTeamId;
	
	
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------
	//Scenario: Adding a calendar entry to a calendar
	
	
	
	@Given("^a calendar containing an entry with start date and time \"(.*?)\" and duration of (\\d+)$")
	public void a_calendar_containing_an_entry_with_start_date_and_time_and_duration_of(String startDateTime, int duration) throws Throwable {	
		
		cal = new Calendar();
		
		cal.addEntry(1,startDateTime,duration);

	}
	
	@When("^another entry with a start date and time \"(.*?)\" and duration (\\d+) is added to the calendar$")
	public void another_entry_with_a_start_date_and_time_and_duration_is_added_to_the_calendar(String startDateTime, int duration) throws Throwable {
	    
		response = cal.addEntry(1,startDateTime,duration);
	   
	}

	

	@Then("^the calendar reports that the operation was unsuccessful by returning \"(.*?)\"$")
	public void the_calendar_reports_that_the_operation_was_unsuccessful_by_returning(String result) throws Throwable {
		
		boolean booleanResult = false;
		
		switch(result){
			case "false": booleanResult = false ; break;
			case "true": booleanResult = true; break;
		}
        
	    Assert.assertEquals(response, booleanResult);
	}

	@Then("^the calendar still only contains (\\d+) entries$")
	public void the_calendar_still_only_contains_entries(int entries) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertEquals(entries,cal.getNrOfEntries());
	}
	
	//----------------------------------------------------------------------------------------------------------------------------------------------
	//Scenario: Using a domain service to schedule a repair team to complete a certain work order
	
	
	
	@Given("^a work order domain service for a domain containing a work order for fault id (\\d+)$")
	public void a_work_order_domain_service_for_a_domain_containing_a_work_order_for_fault_id(int faultId) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		repairTeamService = new RepairTeamService(RepositoryFactory.getFactoryInstance().getRepairTeamRepository());
		workOrderService = new WorkOrderService();
		
		workOrderService.injectRepairTeamServiceDependancy(repairTeamService);
		repairTeamService.injectWorkOrderServiceDependancy(workOrderService);
		
		workOrderService.generateWorkOrderForFaultIDAndTypeAsString(faultId,"pothole","low");
		
		workOrderId = workOrderService.getWorkOrderIDForFaultID(1);
	
		
	}

	@Given("^a repair team named \"(.*?)\" is assigned to the work order$")
	public void a_repair_team_named_is_assigned_to_the_work_order(String teamName) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   

		
		repairTeamService.addARepairTeam(teamName);
		
		repairTeamId = repairTeamService.getListOfAllRepairTeamIds().get(0);
		
		workOrderService.assignRepairTeamToWorkOrder(workOrderId, repairTeamId);
		
	}
	

	
	@When("^the work order gets scheduled at start date time \"(.*?)\" and for a duration of (\\d+) hours$")
	public void the_work_order_gets_scheduled_at_start_date_time_and_for_a_duration_of_hours(String dateTimeString, int duration) throws Throwable {
	    
		workOrderService.scheduleDateTimeForAssignedWorkOrder(workOrderId,dateTimeString,duration);
	   
		
	}

	@Then("^the work order domain service reports the work order as \"(.*?)\"$")
	public void the_work_order_domain_service_reports_the_work_order_as(String status) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertEquals(status,workOrderService.getWorkOrderStatus(workOrderId));
	}

	@Then("^the work order domain service reports the work order scheduled date as \"(.*?)\" and for a duration of (\\d+) hours$")
	public void the_work_order_domain_service_reports_the_work_order_scheduled_date_as_and_for_a_duration_of_hours(String dateTimeString, int duration) throws Throwable {
	   
		Assert.assertEquals(dateTimeString,repairTeamService.getScheduledDateTimeForWorkOrder(workOrderId));
		Assert.assertEquals(duration,repairTeamService.getScheduledDurationForWorkOrder(workOrderId));
	}

	
	
	
	
	
	
	
	
}
