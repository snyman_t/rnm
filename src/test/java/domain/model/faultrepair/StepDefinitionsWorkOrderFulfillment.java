package domain.model.faultrepair;

import java.util.List;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.IProfile.PROFILE_ROLE;
import domain.service.faultrepair.FaultServiceProxy;
import domain.service.faultrepair.WorkOrderServiceProxy;

public class StepDefinitionsWorkOrderFulfillment {
	
	@Given("^a user attempts to view outstanding workorder tasks$")
	public void a_user_attempts_to_view_outstanding_workorder_tasks() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		WorkOrder outstandingWorkOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);

		List<RepairTask> taskList = outstandingWorkOrder1.getRepairTaskList().getListOfIncompleteItems();
		
		for (RepairTask element: taskList){
			Assert.assertFalse(element.isComplete());
		}
		
		Assert.assertTrue(taskList.size()==3);
	}

	@When("^a task is completed$")
	public void a_task_is_completed() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		WorkOrder outstandingWorkOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);
		
		int outstandingCount = outstandingWorkOrder1.getRepairTaskList().getListOfIncompleteItems().size();
		Assert.assertEquals(outstandingCount,3);
		
		int taskNumberToSignoff = 2;
		outstandingWorkOrder1.setTaskCompleteByTasknumber(taskNumberToSignoff);
		
		outstandingCount = outstandingWorkOrder1.getRepairTaskList().getListOfIncompleteItems().size();
		
		Assert.assertEquals(outstandingCount,2);
	}

	@Then("^it can be marked off on the tasklist$")
	public void it_can_be_marked_off_on_the_tasklist() throws Throwable {
	    //the test above passes this criteria
	}

	@When("^a task is incomplete$")
	public void a_task_is_incomplete() throws Throwable {
		//incomplete task populated below
	}

	@Then("^it is marked incomplete on the tasklist and a reason is specified$")
	public void it_is_marked_incomplete_on_the_tasklist_and_a_reason_is_specified() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		WorkOrder outstandingWorkOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);
		
		int taskNumberToSignoff=3;
		String incompleteReason = "wrong tools";
		outstandingWorkOrder1.setTaskIncompleteByTasknumberWithReason(taskNumberToSignoff,incompleteReason);
		
		RepairTask task= outstandingWorkOrder1.getRepairTaskByTaskNumber(taskNumberToSignoff);
		
		Assert.assertFalse(task.isComplete());
		Assert.assertTrue(incompleteReason == task.getTaskDetail());
	}

	@When("^a task list is complete$")
	public void a_task_list_is_complete() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		WorkOrder outstandingWorkOrder =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);
		
		int outstandingCount = outstandingWorkOrder.getRepairTaskList().getListOfIncompleteItems().size();
		Assert.assertEquals(outstandingCount,3);
		
		for (int completedTaskNumber=1 ; completedTaskNumber<=3;completedTaskNumber++){
			outstandingWorkOrder.setTaskCompleteByTasknumber(completedTaskNumber);
		}
		
		outstandingCount = outstandingWorkOrder.getRepairTaskList().getListOfIncompleteItems().size();
		Assert.assertEquals(outstandingCount,0);
	}

	@Then("^the workorder is marked as complete and the fault is marked as complete$")
	public void the_workorder_is_marked_as_complete_and_the_fault_is_marked_as_complete() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		WorkOrder outstandingWorkOrder =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);
		
		int outstandingCount = outstandingWorkOrder.getRepairTaskList().getListOfIncompleteItems().size();
		Assert.assertEquals(outstandingCount,3);
		
		for (int completedTaskNumber=1 ; completedTaskNumber<=3;completedTaskNumber++){
			outstandingWorkOrder.setTaskCompleteByTasknumber(completedTaskNumber);
		}
		
		outstandingCount = outstandingWorkOrder.getRepairTaskList().getListOfIncompleteItems().size();
		Assert.assertEquals(outstandingCount,0);
		
		Assert.assertTrue(outstandingWorkOrder.getStatus() == outstandingWorkOrder.getWorkOrderStatusCompleted());
		Assert.assertTrue(AssignedFaultOne.getStatus() == AssignedFaultOne.getFaultStatusComplete());
	}
}
