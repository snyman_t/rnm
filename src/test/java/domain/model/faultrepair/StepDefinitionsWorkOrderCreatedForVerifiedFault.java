package domain.model.faultrepair;
import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.model.faultlogging.PRIORITY;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.SecurityException;
import domain.service.faultlogging.IProfile.PROFILE_ROLE;
import domain.service.faultrepair.WorkOrderService;
import domain.service.faultrepair.WorkOrderServiceProxy;

public class StepDefinitionsWorkOrderCreatedForVerifiedFault {
	
	@Given("^a verified Fault$")
	public void a_Verified_Fault_is_Open() throws Throwable {
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);
	    
	    Assert.assertTrue(verifiedFault.getStatus() == verifiedFault.getFaultStatusVerified());
	}

	@When("^no WorkOrder exists for fault$")
	public void no_WorkOrder_exists() throws Throwable {
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);
		
		Assert.assertFalse(verifiedFault.getStatus() == verifiedFault.getFaultStatusAssignedToWorkOrder());
	}

	@Then("^create a new WorkOrder$")
	public void create_a_new_WorkOrder() throws Throwable {		
		WorkOrderService workOrderService = new WorkOrderService();
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);

		WorkOrder workOrderForFault = workOrderService.generateWorkOrderForFault(verifiedFault);
		
		Assert.assertTrue(workOrderForFault.getStatus() == workOrderForFault.getWorkOrderStatusIssued());
	}
	
	@Given("^a new WorkOrder is created$")
	public void a_new_WorkOrder_is_created() throws Throwable {
		WorkOrderService workOrderService = new WorkOrderService();
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);

		WorkOrder workOrderForFault = workOrderService.generateWorkOrderForFault(verifiedFault);
		
		Assert.assertTrue(workOrderForFault.getStatus() == workOrderForFault.getWorkOrderStatusIssued());
	}

	@When("^a textual task description And no image is exists for fault$")
	public void only_a_textual_description_is_give() throws Throwable {
		RepairTask repairTask1 = new RepairTask(1,"the first thing to do","");
		
		Assert.assertTrue(repairTask1.getAnnotatedImage()=="No_Image");
	}

	@Then("^create a list of repair tasks$")
	public void create_a_list_of_repair_tasks() throws Throwable {
		RepairTask repairTask1 = new RepairTask(1,"the first thing to do","");
		RepairTask repairTask2 = new RepairTask(2,"the second thing to do","");
		RepairTask repairTask3 = new RepairTask(3,"the third thing to do","");
		
		RepairTaskList listOfTasks = new RepairTaskList();
		listOfTasks.addItem(repairTask1);
		listOfTasks.addItem(repairTask2);
		listOfTasks.addItem(repairTask3);
		
		Assert.assertTrue(listOfTasks.getListOfItems().size()==3);
	}
	
	@Given("^a new annotated Task is created$")
	public void a_new_annotated_WorkOrder_is_created() throws Throwable {
		RepairTask repairTask1 = new RepairTask(1,"the first thing to do","Where To Cut.png");
	}

	@When("^a textual task description is given And an AnnotatedImage is provided for fault$")
	public void a_textual_description_is_given_And_an_AnnotatedImage_is_provided() throws Throwable {
		String taskDescription="the first thing to do";
		String taskImage="Where To Cut.png";
		
		RepairTask repairTask1 = new RepairTask(1,taskDescription,taskImage);
		
		Assert.assertTrue(repairTask1.getAnnotatedImage()==taskImage);
	}

	@Then("^create a list of RepairTasks with annotated Images$")
	public void create_a_list_of_RepairTasks_And_an_AnnotatedImage() throws Throwable {
		String taskDescription="the first thing to do";
		String taskImage="Where To Cut.png";
		
		RepairTask repairTask1 = new RepairTask(1,taskDescription,"");
		RepairTask repairTask2 = new RepairTask(2,taskDescription+"next",taskImage);
		RepairTask repairTask3 = new RepairTask(3,"",taskImage);
		
		RepairTaskList listOfTasks = new RepairTaskList();
		listOfTasks.addItem(repairTask1);
		listOfTasks.addItem(repairTask2);
		listOfTasks.addItem(repairTask3);
		
		Assert.assertFalse(repairTask1.getAnnotatedImage()==taskImage);
		Assert.assertTrue(repairTask2.getAnnotatedImage()==taskImage);
		Assert.assertTrue(listOfTasks.getListOfItems().size()==3);
	}
	
	@Given("^a new WorkOrder is successfully created$")
	public void a_new_WorkOrder_is_successfully_created() throws Throwable {
		WorkOrderService workOrderService = new WorkOrderService();
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);

		WorkOrder workOrderForFault = workOrderService.generateWorkOrderForFault(verifiedFault);
		
		Assert.assertTrue(workOrderForFault.getStatus() == workOrderForFault.getWorkOrderStatusIssued());
	}

	@When("^a list of tasks is generated$")
	public void a_list_of_tasks_is_generated() throws Throwable {
		WorkOrderService workOrderService = new WorkOrderService();
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);

		WorkOrder workOrderForFault = workOrderService.generateWorkOrderForFault(verifiedFault);

		Assert.assertTrue(workOrderForFault.getRepairTaskList().getListOfItems().size() > 0);
	}

	@Then("^associate a BillOfMaterials and a BillOfEquipment$")
	public void associate_a_BillOfMaterials_and_a_BillOfEquipment() throws Throwable {
		WorkOrderService workOrderService = new WorkOrderService();
		Fault verifiedFault = new Fault(22,FAULTTYPE.POTHOLE);
		verifiedFault.setPriority(PRIORITY.LOW);

		WorkOrder workOrderForFault = workOrderService.generateWorkOrderForFault(verifiedFault);
	    
	    Assert.assertTrue(workOrderForFault.getBillOfMaterials().getListOfItems().size() > 0);
	    Assert.assertTrue(workOrderForFault.getBillOfEquipment().getListOfItems().size() > 0); 
	}

	@Given("^a new  WorkOrder is to be created$")
	public void a_new_WorkOrder_is_to_be_created() throws Throwable {
		//nothing to do
	}

	@When("^the user has permission to create WorkOrders$")
	public void the_user_has_permission_to_create_WorkOrders() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
	}

	@Then("^display the Option to create a WorkOrder$")
	public void display_the_Option_to_create_a_WorkOrder() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // To be implemented with a user interface
		// tested that user is able to create workorder via proxy service
		// assuming user is authenticated.
		// see UnitWorkServiceProxyTest for more info
		throw new PendingException();
	}

	
}
