package domain.model.faultrepair;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import junit.framework.TestCase;

import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/domain/model/faultrepair/WorkOrderFaultDetailPresentationAndAssignment.feature")
public class RunCucumberWorkOrderFaultDetailPresentationAndAssignment extends TestCase {
	
}



