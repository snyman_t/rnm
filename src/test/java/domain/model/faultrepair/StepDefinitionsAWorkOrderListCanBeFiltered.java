package domain.model.faultrepair;


import java.util.List;







import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.RepairTeamService;
import domain.service.faultrepair.WorkOrderService;




public class StepDefinitionsAWorkOrderListCanBeFiltered {
	
	WorkOrderService workOrderService;
	RepairTeamService repairTeamService;


	// Scenario Outline: Based on fault type
	
	@Given("^a domain service for a domain containing a list of work orders for the following faults with differing types:$")
	public void a_domain_service_for_a_domain_containing_a_list_of_work_orders_for_the_following_faults_with_differing_types(DataTable listOfWorkOrders) throws Throwable {

		workOrderService = new WorkOrderService();
		repairTeamService = new RepairTeamService(RepositoryFactory.getFactoryInstance().getRepairTeamRepository());
		
	    workOrderService.injectRepairTeamServiceDependancy(repairTeamService);
		repairTeamService.injectWorkOrderServiceDependancy(workOrderService);
		
		List<List<String>> entries = listOfWorkOrders.asLists(String.class);
		
		for(List<String> row : entries){
			
			int faultId = Integer.parseInt(row.get(0));
			String faultType = row.get(1);
			String faultPriority = row.get(2);
		
			workOrderService.generateWorkOrderForFaultIDAndTypeAsString(faultId, faultType, faultPriority);

		}

		
	}

	@When("^I request to see only work orders with fault type \"(.*?)\"$")
	public void i_request_to_see_only_work_orders_with_fault_type(String faultType) throws Throwable {
	   List<Integer> ids = workOrderService.getAllWorkOrderIds("ISSUED", "LOW", "POTHOLE");
	   
	}

	@Then("^the fault type of the associated fault for each work order returned should be \"(.*?)\"$")
	public void the_fault_type_of_the_associated_fault_for_each_work_order_returned_should_be(String arg1) throws Throwable {
	   
	}

	
	// Scenario Outline: Based on priority
	
	@Given("^a domain service for a domain containing a list of work orders for the following faults with differing priorities:$")
	public void a_domain_service_for_a_domain_containing_a_list_of_work_orders_for_the_following_faults_with_differing_priorities(DataTable arg1) throws Throwable {

	}
	
	@When("^I request to see only work orders with priority \"(.*?)\"$")
	public void i_request_to_see_only_work_orders_with_priority(String arg1) throws Throwable {

	}

	@Then("^the priority of the associated fault for each work order should be \"(.*?)\"$")
	public void the_priority_of_the_associated_fault_for_each_work_order_should_be(String arg1) throws Throwable {

	}
	

}
