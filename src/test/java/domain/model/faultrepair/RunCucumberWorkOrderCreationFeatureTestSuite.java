package domain.model.faultrepair;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import junit.framework.TestCase;

import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/domain/model/faultrepair/WorkOrderCreationForVerifiedFault.feature")
public class RunCucumberWorkOrderCreationFeatureTestSuite extends TestCase {
	
}



