package domain.model.faultrepair;



import java.util.ArrayList;
import java.util.List;






import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.RepairTeamService;
import domain.service.faultrepair.WorkOrderService;

public class StepDefinitionsADateAndTimeCanBeAssignedToWorkOrderAutomatically {
	
	WorkOrderService workOrderService;
	RepairTeamService repairTeamService;
	
	int repairTeamId;
	int workOrderId;
	String currentDateTime;
	
	
	@Given("^a work order domain service for a domain containing a work order issued for a \"(.*?)\" priority fault$")
	public void a_work_order_domain_service_for_a_domain_containing_a_work_order_issued_for_a_priority_fault(String priority) throws Throwable {
	   
		workOrderService = new WorkOrderService();
		repairTeamService = new RepairTeamService(RepositoryFactory.getFactoryInstance().getRepairTeamRepository());
		
	    workOrderService.injectRepairTeamServiceDependancy(repairTeamService);
		repairTeamService.injectWorkOrderServiceDependancy(workOrderService);
	    
		workOrderService.generateWorkOrderForFaultIDAndTypeAsString(100, "pothole",priority);
		
		workOrderId  = workOrderService.getWorkOrderIDForFaultID(100);
	}
	
	@Given("^the work order has a repair team assigned to it$")
	public void the_work_order_has_a_repair_team_assigned_to_it() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		
		repairTeamService.addARepairTeam("Team A");
	    repairTeamId = repairTeamService.getRepairTeamId("Team A");
	    
	    workOrderService.assignRepairTeamToWorkOrder(workOrderId, repairTeamId);
	    
	}

	@Given("^the repair team's calendar has multiple other entries:$")
	public void the_repair_team_s_calendar_has_multiple_other_entries(DataTable calendarEntries) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
	    
		List<List<String>> entries = calendarEntries.asLists(String.class);
		
		//String entr = entries.get(0).get(0);
		
		for(List<String> row : entries){
			
			int faultId = Integer.parseInt(row.get(0));
			String faultType = row.get(1);
			String faultPriority = row.get(2);
			String startDateTime = row.get(3);
			int duration = Integer.parseInt(row.get(4));
			
			workOrderService.generateWorkOrderForFaultIDAndTypeAsString(faultId, faultType, faultPriority);
			int workOrderId = workOrderService.getWorkOrderIDForFaultID(faultId);
			
			workOrderService.assignRepairTeamToWorkOrder(workOrderId, repairTeamId);
			
		    workOrderService.scheduleDateTimeForAssignedWorkOrder(workOrderId, startDateTime, duration);
		}
		
		
		
		
		//repairTeamService.scheduleDateTimeForRepairTeamId(repairTeamId, 2,, duration)
	}

	@Given("^the current date and time is \"(.*?)\"$")
	public void the_current_date_and_time_is(String currentDateTime) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    this.currentDateTime = currentDateTime;
	}

	@When("^the domain service requests to schedule the work order automatically for a duration of (\\d+) hours$")
	public void the_domain_service_requests_to_schedule_the_work_order_automatically_for_a_duration_of_hours(int duration) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    workOrderService.scheduleDateTimeAutomaticallyForIssuedWorkOrder(workOrderId,currentDateTime,duration);
	    
	}

	@Then("^the work order domain service reports the work order status as \"(.*?)\"$")
	public void the_work_order_domain_service_reports_the_work_order_status_as(String status) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		
		Assert.assertEquals(status,workOrderService.getWorkOrderStatus(workOrderId));
	}

	@Then("^the work order domain service reports the scheduled date and time for the work order as \"(.*?)\"$")
	public void the_work_order_domain_service_reports_the_scheduled_date_and_time_for_the_work_order_as(String startDateTime) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(startDateTime,repairTeamService.getScheduledDateTimeForWorkOrder(workOrderId));
	}
	

}
