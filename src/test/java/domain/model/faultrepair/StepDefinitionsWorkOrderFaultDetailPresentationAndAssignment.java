package domain.model.faultrepair;

import java.util.List;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.SecurityException;
import domain.service.faultlogging.IProfile.PROFILE_ROLE;
import domain.service.faultrepair.FaultServiceProxy;
import domain.service.faultrepair.WorkOrderService;
import domain.service.faultrepair.WorkOrderServiceProxy;

public class StepDefinitionsWorkOrderFaultDetailPresentationAndAssignment {
	@Given("^a user attempts to view Faults$")
	public void a_user_attempts_to_view_Faults() throws Throwable {
		//proceed to When clause
	}

	@When("^the user has WorkOrder creation rights And chooses to view faults as a list$")
	public void the_user_has_WorkOrder_creation_rights_And_chooses_to_view_faults_as_a_list() throws Throwable {
		//create profile with workorder creation rights
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		faultServiceProxy.addFault(100,"pothole","low","100reports.rprt");
		faultServiceProxy.addFault(101,"drainage","low","101reports.rprt");
		faultServiceProxy.addFault(102,"trafficlight","low","102reports.rprt");
			
		faultServiceProxy.getAllIssuedFaults();

	}

	@Then("^display the faults information as a List and display the fault priority$")
	public void display_the_faults_information_as_a_List_and_display_the_fault_priority() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100reports.rprt");
		faultServiceProxy.addFault(101,"drainage","high","101reports.rprt");
		faultServiceProxy.addFault(102,"trafficlight","high","102reports.rprt");
		
		List <Fault> faultlist = faultServiceProxy.getAllIssuedFaults();
		Assert.assertTrue(faultlist.size()==3);

		for (Fault element : faultlist){
			Assert.assertTrue(element.getFaultPriorityAsString() == "HIGH");
		}
		
	}

	@When("^the user has WorkOrder creation rights And chooses to view faults on a map$")
	public void the_user_has_WorkOrder_creation_rights_And_chooses_to_view_faults_on_a_map() throws Throwable {
		
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		faultServiceProxy.addFault(101,"drainage","normal","101faults.rprt");
		faultServiceProxy.addFault(102,"trafficlight","low","102faults.rprt");
		
		//Stubbing the response with a String to represent the map
		String FaultMap = faultServiceProxy.getMapOfIssuedFaultsWithPriority();
		
		Assert.assertTrue(FaultMap.equals("map/100-HIGH/101-NORMAL/102-LOW"));
	}

	@Then("^display the faults information on a Map And display the fault priority$")
	public void display_the_faults_information_on_a_Map_And_display_the_fault_priority() throws Throwable {
	    //String above will display as "map/100-HIGH/101-NORMAL/102-LOW" in place of map generation
	}

	@Given("^an authorised user is viewing fault$")
	public void an_authorised_user_is_viewing_fault() throws Throwable {
		//proceed to When step below as user authorised there
	}

	@When("^they select the report card$")
	public void they_select_they_select_a_the_report_card() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		faultServiceProxy.addFault(101,"drainage","normal","101faults.rprt");
		faultServiceProxy.addFault(102,"trafficlight","low","102faults.rprt");
		
		int faultId = 101;
		
		String reportCard = faultServiceProxy.getReportCardForFaultId(faultId);
		
		Assert.assertTrue(reportCard.equals("101faults.rprt"));
	}

	@Then("^they are able to see fault reportcard information$")
	public void they_are_able_to_see_fault_reportcard_information() throws Throwable {
	    //TODO implement GUI to display the report returned in the above
	}

	@Given("^a fault has no outstanding WorkOrders$")
	public void a_fault_has_no_outstanding_WorkOrders() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		Assert.assertTrue(AssignedFaultOne.getStatus() == AssignedFaultOne.getFaultStatusVerified());
		
	}

	@When("^there are images on the original fault card$")
	public void there_are_images_on_the_original_fault_card() throws Throwable {
		//fault is passed as is to the workOrder so this field is preserved
		//only the status of the fault is changed, not the content
	}

	@Then("^complete workorder creation And set status to toschedule$")
	public void complete_workorder_creation_And_set_status_to_toschedule() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		WorkOrder outstandingWorkOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);
		WorkOrder outstandingWorkOrder2 =  workOrderServiceProxy.generateWorkOrderForFault(AssignedFaultOne);
		
		Assert.assertTrue(AssignedFaultOne.getStatus() == AssignedFaultOne.getFaultStatusAssignedToWorkOrder());
		Assert.assertTrue(outstandingWorkOrder1.getStatus() == outstandingWorkOrder1.getWorkOrderStatusIssued());
		Assert.assertTrue(outstandingWorkOrder2==null);
	}

	@Given("^a Fault needs to be modified$")
	public void a_Fault_needs_to_be_modified() throws Throwable {
	    //Check and attempt to cahnge the status of the fault in the next step
	}

	@When("^it is already being modified$")
	public void it_is_already_being_modified() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault AssignedFaultOne= faultServiceProxy.getFaultForFaultId(100);
		
		AssignedFaultOne.setStatus(AssignedFaultOne.getFaultStatusModificationInProgress());
		
		Assert.assertTrue(AssignedFaultOne.getStatus() == AssignedFaultOne.getFaultStatusModificationInProgress());
	}

	@Then("^do not allow changes to the fault$")
	public void do_not_allow_changes_to_the_fault() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault faultForModification= faultServiceProxy.getFaultForFaultId(100);
		
		faultForModification.setStatus(faultForModification.getFaultStatusModificationInProgress());
		
		Assert.assertTrue(faultForModification.getStatus() == faultForModification.getFaultStatusModificationInProgress());
		
		WorkOrder workOrder =  workOrderServiceProxy.generateWorkOrderForFault(faultForModification);
		
		Assert.assertTrue(workOrder==null);
	}

	@Given("^a fault has a WorkOrder assigned$")
	public void a_fault_has_a_WorkOrder_assigned() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault assignedFault= faultServiceProxy.getFaultForFaultId(100);

		WorkOrder workOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(assignedFault);
		
		Assert.assertTrue(assignedFault.getStatus() == assignedFault.getFaultStatusAssignedToWorkOrder());
	}

	@When("^the WorkOrder has been verified$")
	public void the_WorkOrder_has_been_verified() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault assignedFault= faultServiceProxy.getFaultForFaultId(100);

		WorkOrder workOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(assignedFault);
		workOrder1.setStatus(workOrder1.getWorkOrderStatusVerified(assignedFault));
		
		Assert.assertTrue(workOrder1.getStatus() == workOrder1.getWorkOrderStatusVerified(assignedFault));
	}

	@Then("^a new WorkOrder can be assigned$")
	public void a_new_WorkOrder_can_be_assigned() throws Throwable {
		Profile profile = new Profile(7, "workOrderCreator", "password", PROFILE_ROLE.WORK_ORDER_CREATOR);
		FaultServiceProxy faultServiceProxy = new FaultServiceProxy(profile);
		WorkOrderServiceProxy workOrderServiceProxy = new WorkOrderServiceProxy(profile);
		
		faultServiceProxy.addFault(100,"pothole","high","100faults.rprt");
		
		Fault assignedFault= faultServiceProxy.getFaultForFaultId(100);

		WorkOrder workOrder1 =  workOrderServiceProxy.generateWorkOrderForFault(assignedFault);
		workOrder1.setStatus(workOrder1.getWorkOrderStatusVerified(assignedFault));
		
		Assert.assertTrue(assignedFault.getStatus() == assignedFault.getFaultStatusVerified());
		
		Assert.assertTrue(workOrder1.getStatus() == workOrder1.getWorkOrderStatusVerified());
		
		WorkOrder workOrder2 =  workOrderServiceProxy.generateWorkOrderForFault(assignedFault);
		
		//fault can be assigned as the previous workorder was verified
		Assert.assertTrue(workOrder2.getStatus() == workOrder2.getWorkOrderStatusIssued());
	}
}
