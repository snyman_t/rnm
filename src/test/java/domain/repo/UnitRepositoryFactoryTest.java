package domain.repo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import domain.model.faultrepair.WorkOrder;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;

public class UnitRepositoryFactoryTest {
	private Repository<WorkOrder> _workOrderRepo;
	
	@Before
	public void upFront(){		
		_workOrderRepo = RepositoryFactory.getFactoryInstance().getWorkOrderRepository();
	}
		
	@Test
	public void canAddAndRetrieveWorkOrders()
	{
		WorkOrder wo1 = new WorkOrder();
		WorkOrder wo2 = new WorkOrder();
		WorkOrder wo3 = new WorkOrder();
		
		_workOrderRepo.add(wo1);
		_workOrderRepo.add(wo2);
		_workOrderRepo.add(wo3);
		
		Assert.assertTrue(_workOrderRepo.getAll().size()==3);
	}
	
}
