package service;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.model.faultlogging.Call;
import domain.model.faultlogging.FAULTSOURCE;
import domain.model.faultlogging.Fault;
import domain.model.faultlogging.FaultLocation;
import domain.service.faultlogging.CallService;
import domain.service.faultlogging.FaultService;
import domain.service.faultlogging.GeoLocationService;
import junit.framework.TestCase;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:14 PM
 */
public class StepDefinitionsFaultLogging extends TestCase {

    private GeoLocationService geoLocationService;
    String inputStreetName;
    String inputStreetSuburb;
    private int inputXCoordincate;
    private int inputYCoordincate;
    private Fault newFault;
    private Call newCall;
    private Fault matchedFault;
    private Call matchedCall;
    private FaultService faultService;
    private CallService callService;
    private int queryFaultID;
    private int queryXCoOrdinate;
    private int queryYCoOrdinate;
    private List<Fault> nearByFaultList;
    private FAULTSOURCE currentFaultSource;

    @Before
    public void beforeTest() {
        faultService = new FaultService();
        callService = new CallService();
        geoLocationService = new GeoLocationService();
        // Default
        this.currentFaultSource = FAULTSOURCE.CALL_CENTRE;
    }

    @Given("^A Fault exists with type='(.+)'; description='(.+)'; StreetName='(.+)'; StreetSuburb='(.+)'; xCoordinate='(\\d+)'; yCoordinate='(\\d+)'$")
    public void a_fault_exists_with_type(String type, String description, String streetName, String streetSuburb, int xCoordinate, int yCoordinate) throws Throwable {
        //Initialise Service
//        faultService = new FaultService();
        newFault = new Fault();
        newFault.setType(Fault.StringToFaultType(type));
        newFault.setDescription(description);
        newFault.setFaultLocation(new FaultLocation(streetName, streetSuburb, xCoordinate, yCoordinate));
    }

    @When("^Call-Centre Agent logs the fault$")
    public void call_Centre_Agent_logs_the_fault() throws Throwable {
        this.currentFaultSource = FAULTSOURCE.CALL_CENTRE;
    }

    @When("^Fault inspector Mobile App logs the fault$")
    public void fault_inspector_Mobile_App_logs_the_fault() throws Throwable {
          this.currentFaultSource = FAULTSOURCE.MOBILE_APP;
    }

    @Given("^a Member calls to report the fault$")
    public void a_Member_calls_to_report_the_fault() throws Throwable {
        // Nothing to action - can ignore
    }

    @Given("^The fault database has multiple entries:$")
    public void the_fault_database_has_multiple_entries(DataTable dataTable) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)

        faultService = new FaultService();
        List<List<String>> entries = dataTable.asLists(String.class);
        boolean afterFirstRow = false;
        for (List<String> row : entries) {
            if (afterFirstRow) {
                newFault = new Fault();
                newFault.setId(Integer.parseInt(row.get(0)));
                newFault.setFaultID(newFault.getId());
                newFault.setType(Fault.StringToFaultType(row.get(1)));
                newFault.setDescription(row.get(2));
                String stringLocationStreet = row.get(3);
                String stringLocationSuburb = row.get(4);
                newFault.setPriority(Fault.StringToPriority(row.get(5)));
                newFault.setStatus(Fault.StringToFaultStatus(row.get(6)));
                int xCoOrd = Integer.parseInt(row.get(7));
                int yCoOrd = Integer.parseInt(row.get(8));
                newFault.setSource(this.currentFaultSource);
                newFault.setFaultLocation(new FaultLocation(stringLocationStreet, stringLocationSuburb, xCoOrd, yCoOrd));
                // Add new Fault to database
                faultService.addNewFault(newFault);
            } else {
                afterFirstRow = true;
            }
        }
    }


    @When("^faults are logged by Call Centre Operator$")
    public void faults_are_loggedByCallCentreOperator() throws Throwable {
        //Override the Source
        this.currentFaultSource = FAULTSOURCE.CALL_CENTRE;
    }

    @When("^faults are logged by Fault Investigator$")
    public void faults_are_loggedByFaultInvestigator() throws Throwable {
        //Override the Source
        this.currentFaultSource = FAULTSOURCE.MOBILE_APP;
    }

    @When("^fault inspector updates coordinates of fault with ID='(\\d+)' to have xCoordinate='(\\d+)'; yCoordinate='(\\d+)'$")
    public void fault_inspector_updates_coordinates_of_fault_with_ID_to_have_xCoordinate_yCoordinate(int faultID, int newXCoordinate, int newYCoordinate) throws Throwable {
        faultService.updateFaultGeoLocation(faultID, newXCoordinate, newYCoordinate);
    }

    @When("^the new fault is logged with priority='(.+)', status='(.+)'$")
    public void call_Centre_Agent_logs_a_new_fault(String priority, String status) throws Throwable {
        // Set Defaults
        newFault.setSource(this.currentFaultSource);
        // Map Priority
        newFault.setPriority(Fault.StringToPriority(priority));
        // Map Fault Status
        newFault.setStatus(Fault.StringToFaultStatus(status));
        // Generate new Fault ID
        newFault.setFaultID(faultService.generateNewFaultID());
        newFault.setId((int) newFault.getFaultID());
        // Add new Fault to database
        faultService.addNewFault(newFault);


        // Initialize Call
        newCall = new Call();
        newCall.setStatus(Call.CALLSTATUS.NEW);
        newCall.setAuditDate(new Date());
        newCall.setLinkedFaultID(newFault.getFaultID());
        newCall.setId((int) callService.generateNewCallID());
        newCall.setCallerReferenceID(callService.generateNewCallReferenceID(newCall.getId()));
        callService.addNewCall(newCall);
    }

    @When("^there is a call query for Geocoded XCoOrdinate='(\\d+)'; YCoOrdinate='(\\d+)'$")
    public void there_is_a_call_query_for_StreetGeoLocation(int xCoOrdinate, int yCoOrdinate) throws Throwable {
        this.queryXCoOrdinate = xCoOrdinate;
        this.queryYCoOrdinate = yCoOrdinate;
        matchedFault = faultService.getFaultDetailsByGeoCode(this.queryXCoOrdinate, this.queryYCoOrdinate);
    }

    @When("^there is a call query for FaultID='(\\d+)'$")
    public void there_is_a_call_query_for_FaultID(int queryFaultID) throws Throwable {
        this.queryFaultID = queryFaultID;
        matchedFault = faultService.getFaultDetailsByID(this.queryFaultID);
    }

    @Then("^the fault found has values ID='(\\d+)'; type='(.+)'; description='(.+)'; StreetName='(.+)'; Suburb='(.+)'; priority='(.+)'; status='(.+)'; source='(.+)'; xCoordinate='(\\d+)'; yCoordinate='(\\d+)'$")
    public void the_fault_found_has_values(int faultID, String type, String description, String streetName, String streetSuburb, String priority, String status, String source, int xCoordinate, int yCoordinate) throws Throwable {
        assertEquals(matchedFault.getId(), faultID);
        assertEquals(matchedFault.getDescription(), description);
        assertEquals(matchedFault.getFaultLocation().getStreetName(), streetName);
        assertEquals(matchedFault.getFaultLocation().getStreetSuburb(), streetSuburb);
        assertEquals(matchedFault.getFaultLocation().getSimpleXCoordinate(), xCoordinate);
        assertEquals(matchedFault.getFaultLocation().getSimpleYCoordinate(), yCoordinate);
        assertEquals(matchedFault.getPriority(), Fault.StringToPriority(priority));
        assertEquals(matchedFault.getStatus(), Fault.StringToFaultStatus(status));
        assertEquals(matchedFault.getSource(), Fault.StringToSource(source));
    }

    @Then("^Fault database size should be (\\d+)")
    public void check_fault_database_size(int expectedSize) throws Throwable {
        int actualSize = faultService.getAllOpenFaults().size();
        assertEquals(actualSize, expectedSize);
    }


    @Then("^fault database with ID='(\\d+)' must have values type='(.+)'; description='(.+)'; StreetName='(.+)'; Suburb='(.+)'; priority='(.+)'; status='(.+)'; source='(.+)'; xCoordinate='(\\d+)'; yCoordinate='(\\d+)'$")
    public void fault_database_with_ID_must_have_values(int faultID, String type, String description, String streetName, String streetSuburb, String priority, String status, String source, int xCoordinate, int yCoordinate) throws Throwable {
        matchedFault = faultService.getFaultDetailsByID(faultID);
        assertEquals(matchedFault.getDescription(), description);
        assertEquals(matchedFault.getFaultLocation().getStreetName(), streetName);
        assertEquals(matchedFault.getFaultLocation().getStreetSuburb(), streetSuburb);
        assertEquals(matchedFault.getFaultLocation().getSimpleXCoordinate(), xCoordinate);
        assertEquals(matchedFault.getFaultLocation().getSimpleYCoordinate(), yCoordinate);
        assertEquals(matchedFault.getPriority(), Fault.StringToPriority(priority));
        assertEquals(matchedFault.getStatus(), Fault.StringToFaultStatus(status));
        assertEquals(matchedFault.getSource(), Fault.StringToSource(source));
    }


    @Then("^Call database size should be (\\d+)$")
    public void call_database_size_should_be(int expectedSize) throws Throwable {
        int actualSize = callService.getAllCalls().size();
        assertEquals(actualSize, expectedSize);
    }

    @Then("^Call database with ID='(\\d+)' must have values callReferenceID='(.+)'; callState='(.+)'; faultLinkage='(\\d+)'$")
    public void call_database_with_ID_must_have_values(int aCallID, String aCallReferenceID, String aCallState, int aFaultLinkage) throws Throwable {
        matchedCall = callService.getCallDetailsByID(aCallID);
        assertEquals(matchedCall.getCallerReferenceID(), aCallReferenceID);
        assertEquals(matchedCall.getStatus(), Call.StringToFaultStatus(aCallState));
        assertEquals(matchedCall.getLinkedFaultID(), aFaultLinkage);
    }


    /*
   *  ---------- Scenario 3 ----------
   * This scenario derives a geolocation for a given street address
   * */

    @Given("^The geolocation database has multiple entries:$")
    public void the_geolocation_database_has_multiple_entries(DataTable dataTable) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)

        List<List<String>> entries = dataTable.asLists(String.class);
        for (List<String> row : entries) {
            FaultLocation location = new FaultLocation();
            location.setStreetName(row.get(0));
            location.setStreetSuburb(row.get(1));
            location.setSimpleXCoordinate(Integer.parseInt(row.get(2)));
            location.setSimpleYCoordinate(Integer.parseInt(row.get(3)));
            geoLocationService.add(location);
        }
    }

    @When("^search input is StreetName='(.+)' and Suburb='(.+)'$")
    public void search_input_to_geolocation_database_is_StreetAddress(String aStreetName, String aStreetSuburb) throws Throwable {
        this.inputStreetName = aStreetName;
        this.inputStreetSuburb = aStreetSuburb;
    }

    @When("^search input is XCoordinate='(\\d+)' and YCoordinate='(\\d+)'$")
    public void search_input_to_geolocation_database_is_coordinates(int aInputXCoordincate, int aInputYCoordincate) throws Throwable {
        this.inputXCoordincate = aInputXCoordincate;
        this.inputYCoordincate = aInputYCoordincate;
    }

    @Then("^search result by Street Address is XCoordinate='(\\d+)' and YCoordinate='(\\d+)'$")
    public void search_result_is_Coordinates(int expectedXCoordincate, int expectedYCoordincate) throws Throwable {
        FaultLocation searchLocation = geoLocationService.searchByStreetAddress(this.inputStreetName, this.inputStreetSuburb);
        assertNotNull(searchLocation);
        assertEquals(expectedXCoordincate, searchLocation.getSimpleXCoordinate());
        assertEquals(expectedYCoordincate, searchLocation.getSimpleYCoordinate());

    }

    @Then("^search result by Street GeoLocation is StreetName='(.+)' and Suburb='(.+)'$")
    public void search_result_is_StreetLocation(String expectedStreetName, String expectedStreetSuburb) throws Throwable {
        FaultLocation searchLocation = geoLocationService.searchByCoordinate(this.inputXCoordincate, this.inputYCoordincate);
        assertNotNull(searchLocation);
        assertEquals(expectedStreetName, searchLocation.getStreetName());
        assertEquals(expectedStreetSuburb, searchLocation.getStreetSuburb());

    }

    @Then("^NearbyFaults searched by street address is (\\d+)$")
    public void the_NearbyFaultList_size_is(int expectedSize) throws Throwable {
        nearByFaultList = faultService.getNearbyFaultsByStreetAddress(inputStreetName, inputStreetSuburb);
        assertNotNull(nearByFaultList);
        assertEquals(expectedSize, nearByFaultList.size());
    }

    @Then("^NearbyFaults searched by GeoLocation (\\d+)$")
    public void nearbyfaults_searched_by_GeoLocation(int expectedSize) throws Throwable {
        System.out.println("\nfaultService.getNearbyFaultsByGeoLocation");
        nearByFaultList = faultService.getNearbyFaultsByGeoLocation(inputXCoordincate, inputYCoordincate, FaultService.MAX_DISTANCE_ALLOWED);
        assertNotNull(nearByFaultList);
        assertEquals(expectedSize, nearByFaultList.size());
    }

    @Then("^NearbyFaultList contains FaultID='(\\d+)'$")
    public void nearbyfaultlist_contains_FaultID(int expectedFaultID) throws Throwable {
        boolean foundFaultInList = false;
        for (Iterator<Fault> iterator = nearByFaultList.iterator(); iterator.hasNext();) {
            Fault listItem = iterator.next();
            if (listItem.getFaultID() == expectedFaultID) {
                foundFaultInList = true;
            }
        }
        assertTrue(foundFaultInList);
    }

}