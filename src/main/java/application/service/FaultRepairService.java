package application.service;

import java.util.ArrayList;
import java.util.List;

import domain.model.faultrepair.WorkOrder;
import domain.repo.RepositoryFactory;
import domain.service.faultrepair.FaultServiceProxy;
import domain.service.faultrepair.RepairTeamService;
import domain.service.faultrepair.WorkOrderService;

public class FaultRepairService {
	
	RepairTeamService repairTeamService;
	WorkOrderService workOrderService;
	FaultServiceProxy faultService;
	
	public FaultRepairService(){
		repairTeamService = new RepairTeamService(RepositoryFactory.getFactoryInstance().getRepairTeamRepository()); 
		workOrderService = new WorkOrderService(RepositoryFactory.getFactoryInstance().getWorkOrderRepository());
	}
	
	public List<WorkOrder> getAllIssuedWorkOrder(){
		return workOrderService.getAllIssuedWorkOrder();
	}
	
	public List<WorkOrder> getAllAssigndWorkOrders(){
		return workOrderService.getAllAssigndWorkOrders();	
	}
	
	public List<WorkOrder> getAllScheduledWorkOrders(){
		return workOrderService.getAllScheduledWorkOrders();
	}
	
	public void assignRepairTeamToWorkOrder(int workOrderId, int repairTeamId){
		workOrderService.assignRepairTeamToWorkOrder(workOrderId, repairTeamId);
	}
	
	public boolean scheduleDateTimeForAssignedWorkOrder(String dateTimeString){
		return false;	
	}
	
	public boolean scheduleDateTimeAutomaticallyForIssuedWorkOrder(int WorkOrderId){
		return false;
	}
	
	public boolean delayWorkOrderByHours(int WorkOrderId,int hourCount){
		return false;
	}

	public int getAssignedRepairTeamIdForWorkOrder(int workOrderId) {
		return workOrderService.getAssignedRepairTeamIdForWorkOrder(workOrderId);
	}

	public String getWorkOrderStatus(int workOrderId) {
		return workOrderService.getWorkOrderStatus(workOrderId);
	}

}
