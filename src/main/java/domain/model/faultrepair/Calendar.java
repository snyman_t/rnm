package domain.model.faultrepair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

public class Calendar{
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	List<CalendarEntry> entries = new ArrayList<>();
	
	private void forceAddEntry(CalendarEntry newEntry){
		
		
		CalendarEntry clashingEntry = this.getClashingEntry(newEntry);
			
		this.removeEntry(clashingEntry.getWorkOrderId());
				
		this.addEntry(newEntry);
		if(!this.addEntry(new CalendarEntry(clashingEntry.getWorkOrderId(),newEntry.getEndDateTime(),clashingEntry.getDuration()))){
			this.forceAddEntry(new CalendarEntry(clashingEntry.getWorkOrderId(),newEntry.getEndDateTime(),clashingEntry.getDuration()));
		}
		
	}
	
	private CalendarEntry getEntry(int workOrderId){
		
		for(CalendarEntry entry : entries){
			if(entry.getWorkOrderId() == workOrderId){
				return entry;
			}
		}
		
		return null;
	}
	
	private boolean addEntry(CalendarEntry entry){
		
		return this.addEntry(entry.getWorkOrderId(),formatter.format(entry.getStartDateTime()) ,entry.getDuration());
		
		
	}
	
	private CalendarEntry getClashingEntry(CalendarEntry newEntry){
		
		for(int k = 0; k< entries.size();k++){
			if(entries.get(k).clash(newEntry)){
				return entries.get(k);
			}
		}
		
		return null;
	}
	
	public Calendar(){
		formatter.setLenient(false);
	}
	
	public boolean available(String startDateTime, int duration){
		
		try{
			CalendarEntry newEntry = new CalendarEntry(1,formatter.parse(startDateTime),duration);
			
			for(CalendarEntry entry : entries){
				if(entry.clash(newEntry)){
				return false;
				}
			}
		
			return true;
	
		}catch(ParseException pe){
			
			System.out.println("Parse error!!!!");
			return true;
		}
	}
	
	public boolean addEntry(int workOrderId,String startDateTime, int duration){
		
		try{
		
			CalendarEntry newEntry = new CalendarEntry(workOrderId,formatter.parse(startDateTime),duration);
			
			for(CalendarEntry existingEntry : entries){
				
				if(existingEntry.clash(newEntry)){
					return false;
				}
			}
			
			entries.add(newEntry);
		
		}catch(ParseException pe){
			System.out.println("Parse error!!!");
			return false;
		}
		return true;
	}
	
	
	
	
	public int getNrOfEntries(){
		return entries.size();
	}
	
	
	
	public void removeEntry(int workOrderId){
		
		for(int k = 0; k< entries.size();k++){
			if(entries.get(k).getWorkOrderId() == workOrderId){
				entries.remove(k);
			}
		}
	}


	public String getScheduledDateTimeForWorkOrderId(int workOrderId) {
		
		for(CalendarEntry entry : entries){
			if(entry.getWorkOrderId() == workOrderId){
				return formatter.format(entry.getStartDateTime());
			}
		}
		
		return null;
	}
	
	public int getScheduledDurationForWorkOrderId(int workOrderId) {
		
		for(CalendarEntry entry : entries){
			if(entry.getWorkOrderId() == workOrderId){			
				return entry.getDuration();
			}
		}
		
		return 0;
	}

	public String earliestOpening(String currentDateTime, int duration){
		try{
		//The earliest opening (date and time) could potentially be the current date and time 
		Date possibleDate = formatter.parse(currentDateTime);
		
		
		// Iterate through each of the calendar's entries
		for(CalendarEntry entry : entries){
			
			//For each entry attempt a maximum of a 1000 times to find a entry that does not clash with it   
			for(int k = 0; k < 1000; k++){
				
				//Create a new calendar entry for this possible date
				CalendarEntry newEntry = new CalendarEntry(1,possibleDate,duration);
				
				//Check if it clashes with the current entry
				if(!entry.clash(newEntry)){	
					//If it does not clash, move on and try the next calendar entry
					break;
				}
				
				//Each time increase the possible date and time by 1 hour
				possibleDate = DateUtils.addHours(possibleDate, 1);
				
			}
		}
		
		return formatter.format(possibleDate);
	
		}catch(ParseException pe){
			System.out.println("Parse error!!!!");
			return null;
		}
		
	}
	
	
	public void delayWorkOrderEntry(int workOrderId, int hourCount){
		//CalendarEntry entry = this.getCalendarEntryForWorkOrderId(workOrderId);
		
		CalendarEntry entry = this.getEntry(workOrderId);
		this.removeEntry(workOrderId);
		
		Date newStartTime = DateUtils.addHours(entry.getStartDateTime(), hourCount);
		CalendarEntry newEntry = new CalendarEntry(workOrderId,newStartTime,entry.getDuration());
		
		if(!this.addEntry(newEntry)){
			this.forceAddEntry(newEntry);
		}
		
	}

}
