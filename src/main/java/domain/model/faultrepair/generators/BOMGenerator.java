package domain.model.faultrepair.generators;

import domain.model.faultrepair.BillOfMaterials;
import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultrepair.RepairItem;


public class BOMGenerator {
		
		public BillOfMaterials generateBillOfMaterialsForFaultType(FAULTTYPE faulttype){
			BillOfMaterials taskListForFaultType = new BillOfMaterials();
			
			switch(faulttype){
				case POTHOLE: taskListForFaultType=generatePotholeBOM();
					break;
				case DRAINAGE: taskListForFaultType=generateDrainageBOM();
					break;
				case TRAFFICLIGHT:taskListForFaultType=generateTrafficLightBOM();
					break;
				case ROADMARKING:taskListForFaultType=generateRoadMarkingBOM();
					break;
				case ACCIDENT:taskListForFaultType=generateAccidentBOM();
					break;
				case SIGNAGE:taskListForFaultType=generateSignageBOM();
					break;
			}
			return taskListForFaultType;
		}

		private BillOfMaterials genericBOM(){
			BillOfMaterials BOM = new BillOfMaterials();
			
			RepairItem BOMItem1 = new RepairItem(1,"unit","Spade");
		    RepairItem BOMItem2 = new RepairItem(2,"unit","TarBrush");
		    RepairItem BOMItem3 = new RepairItem(5,"unit","Wrenches");
		    
		    BOM.addItem(BOMItem1);
		    BOM.addItem(BOMItem2);
		    BOM.addItem(BOMItem3);
			
			return BOM;
		}
		
		private BillOfMaterials generatePotholeBOM(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOM(); 
		}
		
		private BillOfMaterials generateDrainageBOM(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOM(); 
		}

		private BillOfMaterials generateTrafficLightBOM(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOM();
		}
		
		private BillOfMaterials generateRoadMarkingBOM(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOM();
		}
		
		private BillOfMaterials generateAccidentBOM(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOM();
		}

		private BillOfMaterials generateSignageBOM(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOM();
		}
}
