package domain.model.faultrepair.generators;

import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultrepair.RepairTask;
import domain.model.faultrepair.RepairTaskList;


public class RepairTaskListGenerator {
	
	public RepairTaskList generateTaskListForFaultType(FAULTTYPE faulttype){
		RepairTaskList taskListForFaultType = new RepairTaskList();
		
		switch(faulttype){
			case POTHOLE: taskListForFaultType=generatePotholeTaskList();
				break;
			case DRAINAGE: taskListForFaultType=generateDrainageTaskList();
				break;
			case TRAFFICLIGHT:taskListForFaultType=generateTrafficLightTaskList();
				break;
			case ROADMARKING:taskListForFaultType=generateRoadMarkingTaskList();
				break;
			case ACCIDENT:taskListForFaultType=generateAccidentTaskList();
				break;
			case SIGNAGE:taskListForFaultType=generateSignageTaskList();
				break;
		}
		return taskListForFaultType;
	}

	private RepairTaskList genericTaskList(){
		String taskDescription="the first thing to do";
		String taskImage="Annotation.png";
		
		RepairTask repairTask1 = new RepairTask(1,taskDescription,"");
		RepairTask repairTask2 = new RepairTask(2,taskDescription+"next",taskImage);
		RepairTask repairTask3 = new RepairTask(3,"",taskImage);
		
		RepairTaskList listOfTasks = new RepairTaskList();
		listOfTasks.addItem(repairTask1);
		listOfTasks.addItem(repairTask2);
		listOfTasks.addItem(repairTask3);
		
		return listOfTasks;
	}
	
	private RepairTaskList generatePotholeTaskList(){
		//as different tasks for this faulttype get determined, implement this functionality
		//or populate form database
		return genericTaskList(); 
	}
	
	private RepairTaskList generateDrainageTaskList(){
		//as different tasks for this faulttype get determined, implement this functionality
		//or populate form database
		return genericTaskList(); 
	}

	private RepairTaskList generateTrafficLightTaskList(){
		//as different tasks for this faulttype get determined, implement this functionality
		//or populate form database
		return genericTaskList();
	}
	
	private RepairTaskList generateRoadMarkingTaskList(){
		//as different tasks for this faulttype get determined, implement this functionality
		//or populate form database
		return genericTaskList();
	}
	
	private RepairTaskList generateAccidentTaskList(){
		//as different tasks for this faulttype get determined, implement this functionality
		//or populate form database
		return genericTaskList();
	}

	private RepairTaskList generateSignageTaskList(){
		//as different tasks for this faulttype get determined, implement this functionality
		//or populate form database
		return genericTaskList();
	}
}
