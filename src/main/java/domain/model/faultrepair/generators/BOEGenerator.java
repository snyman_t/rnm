package domain.model.faultrepair.generators;

import domain.model.faultrepair.BillOfEquipment;
import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultrepair.RepairItem;


public class BOEGenerator {
		
		public BillOfEquipment generateBillOfEquipmentForFaultType(FAULTTYPE faulttype){
			BillOfEquipment taskListForFaultType = new BillOfEquipment();
			
			switch(faulttype){
				case POTHOLE: taskListForFaultType=generatePotholeBOE();
					break;
				case DRAINAGE: taskListForFaultType=generateDrainageBOE();
					break;
				case TRAFFICLIGHT:taskListForFaultType=generateTrafficLightBOE();
					break;
				case ROADMARKING:taskListForFaultType=generateRoadMarkingBOE();
					break;
				case ACCIDENT:taskListForFaultType=generateAccidentBOE();
					break;
				case SIGNAGE:taskListForFaultType=generateSignageBOE();
					break;
			}
			return taskListForFaultType;
		}

		private BillOfEquipment genericBOE(){
			BillOfEquipment BOE = new BillOfEquipment();
			
			RepairItem BOEItem1 = new RepairItem(1,"unit","Spade");
		    RepairItem BOEItem2 = new RepairItem(2,"unit","TarBrush");
		    RepairItem BOEItem3 = new RepairItem(5,"unit","Wrenches");
		    
		    BOE.addItem(BOEItem1);
		    BOE.addItem(BOEItem2);
		    BOE.addItem(BOEItem3);
			
			return BOE;
		}
		
		private BillOfEquipment generatePotholeBOE(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOE(); 
		}
		
		private BillOfEquipment generateDrainageBOE(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOE(); 
		}

		private BillOfEquipment generateTrafficLightBOE(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOE();
		}
		
		private BillOfEquipment generateRoadMarkingBOE(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOE();
		}
		
		private BillOfEquipment generateAccidentBOE(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOE();
		}

		private BillOfEquipment generateSignageBOE(){
			//as different tasks for this faulttype get determined, implement this functionality
			//or populate form database
			return genericBOE();
		}
}
