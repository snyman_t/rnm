package domain.model.faultrepair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class CalendarEntry {
	

	private int workOrderId;
	private int duration;
	private Date startDateTime;
	private Date endDateTime;
	
	public CalendarEntry(int workOrderId,Date startDateTime, int duration){
		
		this.workOrderId = workOrderId;
		
		this.startDateTime = startDateTime;
		this.duration = duration;
		
		this.calculateEndDateTime();
		
	}
	
	private void calculateEndDateTime() {
		endDateTime = DateUtils.addHours(startDateTime,duration);
	}

	public Date getStartDateTime() {
		return startDateTime;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public Date getEndDateTime(){
		return endDateTime;
		
	}
	
	public int getWorkOrderId(){
		return workOrderId;
	}
	
	public boolean clash(CalendarEntry otherEntry){
		
		if(endDateTime.getTime() > otherEntry.getStartDateTime().getTime() && startDateTime.getTime() < otherEntry.getEndDateTime().getTime()){
			return true;
		}
		else{
			return false;
		}
	}






	

	

}
