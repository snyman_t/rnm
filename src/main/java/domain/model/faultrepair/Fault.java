package domain.model.faultrepair;

import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultlogging.PRIORITY;
import domain.model.faultrepair.faultstatuses.FaultStatusAssignedToWorkOrder;
import domain.model.faultrepair.faultstatuses.FaultStatusComplete;
import domain.model.faultrepair.faultstatuses.FaultStatusModificationInProgress;
import domain.model.faultrepair.faultstatuses.FaultStatusNotVerified;
import domain.model.faultrepair.faultstatuses.FaultStatusVerified;
import domain.repo.SystemPersistableEntity;


//Steven
public class Fault extends SystemPersistableEntity{
    private boolean _validLocation = false;
    private FAULTTYPE _faultType;
    private PRIORITY _faultPriority;
    private int _faultId;
    String _reportCard;

    
    public Fault(int faultId,FAULTTYPE faulttype) {
    	this.setId(faultId);
    	_faultId = faultId;
    	this._faultType = faulttype;
    	this._faultPriority = PRIORITY.LOW;
    	InstantiateFaultStatuses();
    	faultstatus = faultStatusVerified;
    }
   
    //----state pattern
    FaultStatus faultStatusAssignedToWorkOrder;
    FaultStatus faultStatusVerified;
    FaultStatus faultStatusModificationInProgress;
    FaultStatus faultStatusNotVerified;
    FaultStatus faultStatusComplete;
    
    FaultStatus faultstatus = faultStatusVerified;
    
    public FaultStatus getFaultStatusAssignedToWorkOrder(){return faultStatusAssignedToWorkOrder;}
    public FaultStatus getFaultStatusVerified(){return faultStatusVerified;}
    public FaultStatus getFaultStatusModificationInProgress(){return faultStatusModificationInProgress;}
    public FaultStatus getFaultStatusNotVerified(){return faultStatusNotVerified;}
    public FaultStatus getFaultStatusComplete(){return faultStatusComplete;}
    
    public void InstantiateFaultStatuses(){
    	faultStatusAssignedToWorkOrder = new FaultStatusAssignedToWorkOrder(this);
    	faultStatusVerified = new FaultStatusVerified(this); 
    	faultStatusModificationInProgress = new FaultStatusModificationInProgress(this);
    	faultStatusModificationInProgress = new FaultStatusComplete(this);
    	faultStatusNotVerified = new FaultStatusNotVerified(this);
    }

    public void setStatus(FaultStatus faultstatus){
    	this.faultstatus = faultstatus;
    }
    
    public String getFaultStatusAsString(){
    	return faultstatus.getFaultStatus();
    }
    
    public FaultStatus getStatus(){
    	return faultstatus;
    }
    //----state pattern
    
    public void setPriority(PRIORITY priority){
    	this._faultPriority = priority;
    }
    
    public String getFaultPriorityAsString(){
    	return String.valueOf(this._faultPriority);
    }
    
    public int getFaultId(){
    	return this._faultId;
    }
    
    public void setReportCard(String reportCard){
    	_reportCard = reportCard;
    }
    
    public String getReportCard(){
    	return _reportCard;
    } 

    public FAULTTYPE getType() {
        return _faultType;
    }
    
    public void setFaultType(FAULTTYPE faultType) {
        this._faultType = faultType;
    }

    public boolean validLocation() {
        return this._validLocation;
    }

    public void validLocation(boolean validLocation) {
        this._validLocation = validLocation;
    }
}
