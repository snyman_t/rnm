package domain.model.faultrepair;

import java.util.ArrayList;
import java.util.List;

import domain.repo.SystemPersistableEntity;

abstract public class RepairItemList extends SystemPersistableEntity{
	protected List<RepairItem> _repairItemList;
	
	public RepairItemList(){
		_repairItemList = new ArrayList<RepairItem>();
	}
	
	public List<RepairItem> getListOfItems(){
		return _repairItemList;
	}
	
	public void addItem(RepairItem item){
		_repairItemList.add(item);
	}
}
