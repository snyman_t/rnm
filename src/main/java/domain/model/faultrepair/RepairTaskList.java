package domain.model.faultrepair;

import java.util.ArrayList;
import java.util.List;

import domain.repo.SystemPersistableEntity;

public class RepairTaskList extends SystemPersistableEntity{
	protected List<RepairTask> _repairTaskList;
	
	public RepairTaskList(){
		_repairTaskList = new ArrayList<RepairTask>();
	}
	
	public void updateTaskList(List<RepairTask> list){
		_repairTaskList = list;		
	}
	
	public List<RepairTask> getListOfItems(){
		return _repairTaskList;
	}
	
	public List<RepairTask> getListOfIncompleteItems(){
		List <RepairTask> incompleteItems = new ArrayList<>();
		
		for (RepairTask element:_repairTaskList){
			if (element.isComplete() == false){
				incompleteItems.add(element);
			}
		}
		
		return incompleteItems;
	}
	
	public boolean checkListCompleted(){
		int incompleteCount = getListOfIncompleteItems().size();
		if (incompleteCount ==0){
			return true;
		}
		return false;
	}
	
	public void addItem(RepairTask item){
		_repairTaskList.add(item);
	}
}
