package domain.model.faultrepair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import domain.repo.SystemPersistableEntity;

public class RepairTeam extends SystemPersistableEntity{
	
	private String name;
	private int id;
	private Calendar cal;
	
	public RepairTeam(String name){
		this.name = name;
		this.cal = new Calendar();
	}

	public String getName(){
		return this.name;
	}
	
	public Calendar getCalendar(){
		return this.cal;
	}

}
