package domain.model.faultrepair;

public enum FAULTTYPE {
        POTHOLE, DRAINAGE, TRAFFICLIGHT, ROADMARKING, ACCIDENT, SIGNAGE
}
