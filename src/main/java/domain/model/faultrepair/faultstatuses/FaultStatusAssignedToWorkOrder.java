package domain.model.faultrepair.faultstatuses;

import domain.model.faultrepair.Fault;

public class FaultStatusAssignedToWorkOrder extends FaultStatusDefault{
	
	public FaultStatusAssignedToWorkOrder(Fault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "ASSIGNED_TO_WORKORDER";
	}
}
