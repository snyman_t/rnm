package domain.model.faultrepair.faultstatuses;

import domain.model.faultrepair.Fault;

public class FaultStatusNotVerified extends FaultStatusDefault{
	
	public FaultStatusNotVerified(Fault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "NOT_VERIFIED";
	}
}
