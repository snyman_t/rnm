package domain.model.faultrepair.faultstatuses;

import domain.model.faultrepair.Fault;

public class FaultStatusVerified extends FaultStatusDefault{
	
	public FaultStatusVerified(Fault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "VERIFIED";
	}
}
