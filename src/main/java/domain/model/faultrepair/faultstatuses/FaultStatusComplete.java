package domain.model.faultrepair.faultstatuses;

import domain.model.faultrepair.Fault;

public class FaultStatusComplete extends FaultStatusDefault{
	
	public FaultStatusComplete(Fault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "COMPLETE";
	}
}
