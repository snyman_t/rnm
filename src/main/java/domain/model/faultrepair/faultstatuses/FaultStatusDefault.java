package domain.model.faultrepair.faultstatuses;

import domain.model.faultrepair.FaultStatus;
import domain.model.faultrepair.Fault;

abstract public class FaultStatusDefault implements FaultStatus{
	Fault injectedFault;
	
	
    abstract public String getFaultStatus();

    public FaultStatusDefault(Fault injectedFault){
    	this.injectedFault = injectedFault;
    }
    
	public void resetStatus(){
		System.out.println("Fault Status reset to NEW");
	}
	
	public void nextStatus(){
		System.out.println("Set fault status to next status");
	}
}
