package domain.model.faultrepair.faultstatuses;

import domain.model.faultrepair.Fault;

public class FaultStatusModificationInProgress extends FaultStatusDefault{
	
	public FaultStatusModificationInProgress(Fault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "MODIFICATION_IN_PROGRESS";
	}
}
