package domain.model.faultrepair;

import java.util.Date;
import java.util.List;

import domain.model.faultrepair.workorderstatuses.WorkOrderStatusAssigned;
import domain.model.faultrepair.workorderstatuses.WorkOrderStatusCompleted;
import domain.model.faultrepair.workorderstatuses.WorkOrderStatusIssued;
import domain.model.faultrepair.workorderstatuses.WorkOrderStatusScheduled;
import domain.model.faultrepair.workorderstatuses.WorkOrderStatusToBeIssued;
import domain.model.faultrepair.workorderstatuses.WorkOrderStatusVerified;
import domain.repo.SystemPersistableEntity;

public class WorkOrder extends SystemPersistableEntity{
	protected BillOfMaterials _billOfMaterials;
	protected BillOfEquipment _billOfEquipment;
	protected RepairTaskList _repairTaskList;
	
	protected Fault _fault;
	
	WorkOrderStatus workOrderStatusToBeIssued;
	WorkOrderStatus workOrderStatusIssued;
	WorkOrderStatus workOrderStatusAssigned;
	WorkOrderStatus workOrderStatusScheduled;
	WorkOrderStatus workOrderStatusCompleted;
	WorkOrderStatus workOrderStatusVerified;
    
	WorkOrderStatus workOrderStatus;
	private int assignedRepairTeamId;
    
	public void setId(int id){
		this._id = id;
	}
	
	public WorkOrderStatus getStatus(){
	    return workOrderStatus;
	}
	
    public void InstantiateFaultStatuses(){
    	workOrderStatusToBeIssued = new WorkOrderStatusToBeIssued(this);
    	workOrderStatusIssued = new WorkOrderStatusIssued(this);
    	workOrderStatusScheduled = new WorkOrderStatusScheduled(this);
    	workOrderStatusAssigned = new WorkOrderStatusAssigned(this);
    	workOrderStatusCompleted = new WorkOrderStatusCompleted(this);
    	workOrderStatusVerified = new WorkOrderStatusVerified(this);
    	workOrderStatus = workOrderStatusToBeIssued;
    }
    
    public String getStatusAsString(){
    	return workOrderStatus.getWorkOrderStatusAsString();
    }
    
    public void setStatus(WorkOrderStatus workorderstatus){
		this.workOrderStatus = workorderstatus;
	}
    
    public WorkOrderStatus getWorkOrderStatusToBeIssued(){return workOrderStatusToBeIssued;}
    public WorkOrderStatus getWorkOrderStatusIssued(){return workOrderStatusIssued;}
    public WorkOrderStatus getWorkOrderStatusScheduled(){return workOrderStatusScheduled;}
    public WorkOrderStatus getWorkOrderStatusAssigned(){return workOrderStatusAssigned;}
    public WorkOrderStatus getWorkOrderStatusCompleted(){return workOrderStatusCompleted;}
    public WorkOrderStatus getWorkOrderStatusVerified(){return workOrderStatusVerified;}
    
    public WorkOrderStatus getWorkOrderStatusVerified(Fault assignedFault){
    	assignedFault.setStatus(assignedFault.getFaultStatusVerified());
    	return workOrderStatusVerified;
    }
    
    public void scheduleWorkOrder(Date datetime){
		workOrderStatus.scheduleWorkOrder(datetime);
	}
    
    public void assignRepairTeam(int repairTeamId){
    	workOrderStatus.assignRepairTeam();
    	
    	assignedRepairTeamId = repairTeamId; 
    	
    }
    
    public void assignBillOfMaterials(BillOfMaterials billOfMaterials){
    	workOrderStatus.assignBillOfMaterials(billOfMaterials);
    }
    
    public void assignBillOfEquipment(BillOfEquipment billOfEquipment){
    	workOrderStatus.assignBillOfEquipment(billOfEquipment);
    }
    
    public void assignTaskList(RepairTaskList tasklist){
    	workOrderStatus.assignRepairTaskList(tasklist);
    }
    
    public BillOfMaterials getBillOfMaterials(){
    	return workOrderStatusToBeIssued.getBillOfMaterials();
    }
    
    public BillOfEquipment getBillOfEquipment(){
    	return workOrderStatusToBeIssued.getBillOfEquipment();
    }
    
    public RepairTaskList getRepairTaskList(){
    	return workOrderStatusToBeIssued.getRepairTaskList();
    }
    
    public int getAssignedFaultID(){
    	return _fault.getId();
    }
    
	//Steven
	public WorkOrder(){
		InstantiateFaultStatuses();
	}
	
	public WorkOrder(Fault fault){
		InstantiateFaultStatuses();
		setWorkOrderStatusForFaultStatus(fault);
		this._fault = fault;
	}
	
	public int getFaultID(){
		return this._fault.getId();
	}
	
	//Steven
	protected void setWorkOrderStatusForFaultStatus(Fault fault){
		if (fault.getFaultStatusAsString()=="ASSIGNED_TO_WORKORDER"){
			workOrderStatus = workOrderStatusAssigned;
		}
		else if (fault.getFaultStatusAsString()=="VERIFIED" && fault.validLocation()){
			workOrderStatus = workOrderStatusIssued;	
		}	
		else workOrderStatus = workOrderStatusToBeIssued;
	}

	public int getAssignRepairTeamId() {
		return assignedRepairTeamId;
	}
	
	public RepairTask getRepairTaskByTaskNumber(int taskNumber){
		List<RepairTask> repairList = workOrderStatusToBeIssued.getRepairTaskList().getListOfItems();
		
		if (taskNumber <= repairList.size()){ 
		
			for (RepairTask element : repairList){
				if (element.taskNumber() == taskNumber){
					return element;
				}
			}
			
		}
		return null;
	}
	
	public void setTaskIncompleteByTasknumberWithReason(int taskNumber,String incompleteReason){
		List<RepairTask> repairList = workOrderStatusToBeIssued.getRepairTaskList().getListOfItems();
		
		if (taskNumber <= repairList.size()){ 
		
			for (RepairTask element : repairList){
				if (element.taskNumber() == taskNumber){
					element.setTaskIncomplete(incompleteReason);
				}
			}
			workOrderStatusToBeIssued.updateTaskList(repairList);
		}
	}
	
	public void setTaskCompleteByTasknumber(int taskNumber){
		List<RepairTask> repairList = workOrderStatusToBeIssued.getRepairTaskList().getListOfItems();
		
		if (taskNumber <= repairList.size()){ 
		
			for (RepairTask element : repairList){
				if (element.taskNumber() == taskNumber){
					element.setTaskComplete();
				}
			}
			workOrderStatusToBeIssued.updateTaskList(repairList);
		}
		
		if (workOrderStatusToBeIssued.getRepairTaskList().checkListCompleted()){
			workOrderStatus = workOrderStatusCompleted;
			_fault.setStatus(_fault.getFaultStatusComplete());
		}
	}
}

