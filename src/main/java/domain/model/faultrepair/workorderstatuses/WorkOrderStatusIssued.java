package domain.model.faultrepair.workorderstatuses;

import java.util.Date;

import domain.model.faultrepair.WorkOrder;

public class WorkOrderStatusIssued extends WorkOrderStatusDefault{
	
	public WorkOrderStatusIssued(WorkOrder workOrder){
		super(workOrder);
	}
	
	public String getWorkOrderStatusAsString(){
		return "ISSUED";
	}
	
	//this should probably throw an exception
		public void assignRepairTeam(){
			System.out.println("WorkOrder has been assigned");
			workOrder.setStatus(workOrder.getWorkOrderStatusAssigned());
		}
}
