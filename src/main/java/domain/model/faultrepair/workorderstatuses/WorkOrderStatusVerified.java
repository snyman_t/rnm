package domain.model.faultrepair.workorderstatuses;

import domain.model.faultrepair.WorkOrder;

public class WorkOrderStatusVerified extends WorkOrderStatusDefault{
	
	public WorkOrderStatusVerified(WorkOrder workOrder){
		super(workOrder);
	}
	
	public String getWorkOrderStatusAsString(){
		return "VERIFIED";
	}
}
