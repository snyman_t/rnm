package domain.model.faultrepair.workorderstatuses;

import java.util.Date;
import java.util.List;

import domain.model.faultrepair.BillOfEquipment;
import domain.model.faultrepair.BillOfMaterials;
import domain.model.faultrepair.RepairTask;
import domain.model.faultrepair.RepairTaskList;
import domain.model.faultrepair.WorkOrder;
import domain.model.faultrepair.WorkOrderStatus;

abstract public class WorkOrderStatusDefault implements WorkOrderStatus{
	WorkOrder workOrder;
	
	protected BillOfMaterials _billOfMaterials;
	protected BillOfEquipment _billOfEquipment;
	protected RepairTaskList _repairTaskList;
	
	abstract public String getWorkOrderStatusAsString();
	
    public WorkOrderStatusDefault(WorkOrder workOrder){
    	this.workOrder = workOrder;
    	_billOfMaterials = new BillOfMaterials();
    	_billOfEquipment = new BillOfEquipment();
    	_repairTaskList = new RepairTaskList();
    }
    
    //consider throwing exception
    public void scheduleWorkOrder(Date scheduleDate){
    	System.out.println("Cannot schedule a"+workOrder.getStatusAsString());
    }
    
    //consider throwing exception
    public void assignRepairTeam(){
    	System.out.println("Cannot assign a workorder with status: "+workOrder.getStatusAsString());
    }
    
    public void updateTaskList(List<RepairTask> list){
    	System.out.println("Cannot update a list with status "+workOrder.getStatusAsString());
    }
    
    //consider throwing exception
    public void assignBillOfMaterials(BillOfMaterials billOfMaterials){
    	System.out.println("Cannot assign a bill of materials to workorder with status"+workOrder.getStatusAsString());
    }
    
    public BillOfMaterials getBillOfMaterials(){
    	return _billOfMaterials;
    }
    
    public BillOfEquipment getBillOfEquipment(){
    	return _billOfEquipment;
    }
    
    public RepairTaskList getRepairTaskList(){
    	return _repairTaskList;
    }
    
    //consider throwing exception
    public void assignBillOfEquipment(BillOfEquipment billOfEquipment){
    	System.out.println("Cannot assign a bill of equipment to workorder with status"+workOrder.getStatusAsString());
    }
    
    //consider throwing exception
    public void assignRepairTaskList(RepairTaskList tasklist){
    	System.out.println("Cannot assign a task listto workorder with status"+workOrder.getStatusAsString());
    }
}
