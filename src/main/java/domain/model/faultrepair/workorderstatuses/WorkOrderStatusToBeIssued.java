package domain.model.faultrepair.workorderstatuses;

import java.util.List;

import domain.model.faultrepair.BillOfEquipment;
import domain.model.faultrepair.BillOfMaterials;
import domain.model.faultrepair.RepairTask;
import domain.model.faultrepair.RepairTaskList;
import domain.model.faultrepair.WorkOrder;

public class WorkOrderStatusToBeIssued extends WorkOrderStatusDefault{
	
	public WorkOrderStatusToBeIssued(WorkOrder workOrder){
		super(workOrder);
	}
	
	//consider throwing exception
    public void assignBillOfMaterials(BillOfMaterials billOfMaterials){
    	this._billOfMaterials = billOfMaterials;
    }
    
    public void updateTaskList(List<RepairTask> list){
		_repairTaskList.updateTaskList(list);
	}
    
    //consider throwing exception
    public void assignBillOfEquipment(BillOfEquipment billOfEquipment){
    	this._billOfEquipment = billOfEquipment;
    }
    
    //consider throwing exception
    public void assignRepairTaskList(RepairTaskList tasklist){
    	this._repairTaskList = tasklist;
    }
	
	public String getWorkOrderStatusAsString(){
		return "TOBEISSUED";
	}
}
