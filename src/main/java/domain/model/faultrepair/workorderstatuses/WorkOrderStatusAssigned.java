package domain.model.faultrepair.workorderstatuses;

import java.util.Date;

import domain.model.faultrepair.WorkOrder;

public class WorkOrderStatusAssigned extends WorkOrderStatusDefault{
	
	public WorkOrderStatusAssigned(WorkOrder workOrder){
		super(workOrder);
	}
	
	public String getWorkOrderStatusAsString(){
		return "ASSIGNED";
	}
	
	//this should probably throw an exception
	public void scheduleWorkOrder(Date scheduleDate){
		System.out.println("WorkOrder has been scheduled");
		workOrder.setStatus(workOrder.getWorkOrderStatusScheduled());
	}
}
