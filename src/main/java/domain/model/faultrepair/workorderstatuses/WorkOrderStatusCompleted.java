package domain.model.faultrepair.workorderstatuses;

import domain.model.faultrepair.WorkOrder;

public class WorkOrderStatusCompleted extends WorkOrderStatusDefault{
	
	public WorkOrderStatusCompleted(WorkOrder workOrder){
		super(workOrder);
	}
	
	public String getWorkOrderStatusAsString(){
		return "COMPLETED";
	}
}
