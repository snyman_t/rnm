package domain.model.faultrepair.workorderstatuses;

import domain.model.faultrepair.WorkOrder;

public class WorkOrderStatusScheduled extends WorkOrderStatusDefault{
	
	public WorkOrderStatusScheduled(WorkOrder workOrder){
		super(workOrder);
	}
	
	public String getWorkOrderStatusAsString(){
		return "SCHEDULED";
	}
}
