package domain.model.faultrepair;

import java.util.Date;
import java.util.List;

public interface WorkOrderStatus {
	public String getWorkOrderStatusAsString();
	public void scheduleWorkOrder(Date scheduleDate);
	public void assignRepairTeam();
	public void assignRepairTaskList(RepairTaskList tasklist);
	public void assignBillOfMaterials(BillOfMaterials BOM);
	public void assignBillOfEquipment(BillOfEquipment BOE);
	public void updateTaskList(List<RepairTask> list);
	public BillOfMaterials getBillOfMaterials();
    public BillOfEquipment getBillOfEquipment();
    public RepairTaskList getRepairTaskList();
}
