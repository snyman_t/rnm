package domain.model.faultrepair;

import domain.repo.SystemPersistableEntity;

public class RepairTask extends SystemPersistableEntity{
	int _taskNumber;
	String _taskDetail;
	String _repairImage;
	boolean _isTaskComplete;
	
	public RepairTask(int taskNumber,String taskDetail,String repairImage){
		_taskNumber= taskNumber;
		_taskDetail = taskDetail;
		_isTaskComplete = false;
		
		if (!"".equals(repairImage)){_repairImage=repairImage;}
		else{_repairImage="No_Image";}
	}
	
	public boolean isComplete(){
		return _isTaskComplete;
	}
	
	public int taskNumber(){
		return _taskNumber;
	} 
	
	public String getTaskDetail(){
		return _taskDetail;
	}
	
	public void setTaskIncomplete(String incompleteReason){
		this._isTaskComplete=false;
		this._taskDetail = incompleteReason;
	}
	
	public void setTaskComplete(){
		this._isTaskComplete=true;
	}
	
	public String getRepairTaskImage(FAULTTYPE faulttype){
		return "To be implemented";
	}
	
	public String getAnnotatedImage(){
		return _repairImage;
	}
}