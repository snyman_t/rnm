package domain.model.faultlogging;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:15 AM
 */
public class ReportQuestion {

    private int identifierNumber;
    private String questionText;

    public ReportQuestion(int identifierNumber, String questionText) {
        this.identifierNumber = identifierNumber;
        this.questionText = questionText;
    }

    public int getIdentifierNumber() {
        return identifierNumber;
    }

    public void setIdentifierNumber(int identifierNumber) {
        this.identifierNumber = identifierNumber;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }
}
