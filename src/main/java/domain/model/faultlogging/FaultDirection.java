package domain.model.faultlogging;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:28 AM
 */
public class FaultDirection {

    public enum DIRECTION {
        NORTH, SOUTH, EAST, WEST
    }

    private DIRECTION direction;
    private int distance;
}
