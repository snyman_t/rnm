package domain.model.faultlogging;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:15 AM
 */
public class ReportImage {

    private int identifier;
    private Byte[] image;

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }
}
