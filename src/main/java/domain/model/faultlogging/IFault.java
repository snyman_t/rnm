package domain.model.faultlogging;

import java.util.Date;

public interface IFault {
	public long getFaultID();
	public void setFaultID(long faultID);
	public Fault.STATUS getStatus();
	public void setStatus(Fault.STATUS status);
	public String getDescription();
	public void setDescription(String description);
	public FaultLocation getFaultLocation();
	public void setFaultLocation(FaultLocation faultLocation);
	public FAULTTYPE getType();
	public void setType(FAULTTYPE type);
	public PRIORITY getPriority();
	public void setPriority(PRIORITY priority);
	public Date getExpectedDateOfResolution();
	public void setExpectedDateOfResolution(Date expectedDateOfResolution);
	public int getNumberOfQueries();
	public void setNumberOfQueries(int numberOfQueries);
	public FAULTSOURCE getSource();
	public void setSource(FAULTSOURCE source);
}
