package domain.model.faultlogging;

public enum FAULTSOURCE {
	CALL_CENTRE, MOBILE_APP, SMS, EMAIL
}
