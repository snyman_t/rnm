package domain.model.faultlogging;

import domain.model.faultlogging.faultstatuses.FaultStatusAlreadyResolved;
import domain.model.faultlogging.faultstatuses.FaultStatusAssignDifferentDepartment;
import domain.model.faultlogging.faultstatuses.FaultStatusAssignedToWorkOrder;
import domain.model.faultlogging.faultstatuses.FaultStatusDuplicate;
import domain.model.faultlogging.faultstatuses.FaultStatusLogged;
import domain.model.faultlogging.faultstatuses.FaultStatusNew;
import domain.model.faultlogging.faultstatuses.FaultStatusNotExists;
import domain.model.faultlogging.faultstatuses.FaultStatusNotVerified;
import domain.model.faultlogging.faultstatuses.FaultStatusVerified;
import domain.repo.SystemPersistableEntity;


//Steven
public class InjectedFault extends SystemPersistableEntity{
    private boolean _validLocation = false;
    private FAULTTYPE _faultType;

    //---- attempting state pattern INCOMPLETE
    
    FaultStatus faultStatusNew;
    FaultStatus faultStatusLogged;
    FaultStatus faultStatusAssignedToWorkOrder;
    FaultStatus faultStatusVerified;
    FaultStatus faultStatusDuplicate;
    FaultStatus faultStatusNotVerified;
    FaultStatus faultStatusNotExists;
    FaultStatus faultStatusAssignDifferentDepartment;
    FaultStatus faultStatusAlreadyResolved;
    
    FaultStatus faultstatus = faultStatusNew;
    
    public void InstantiateFaultStatuses(){
    	faultStatusNew = new FaultStatusNew(this);
    	faultStatusAssignedToWorkOrder = new FaultStatusAssignedToWorkOrder(this);
    	faultStatusLogged = new FaultStatusLogged(this);
    	faultStatusVerified = new FaultStatusVerified(this); 
    	faultStatusDuplicate = new FaultStatusDuplicate(this);
    	faultStatusNotVerified = new FaultStatusNotVerified(this);
        faultStatusNotExists = new FaultStatusNotExists(this);
        faultStatusAssignDifferentDepartment = new FaultStatusAssignDifferentDepartment(this);
        faultStatusAlreadyResolved = new FaultStatusAlreadyResolved(this);
    }

    public void setStatus(FaultStatus faultstatus){
    	this.faultstatus = faultstatus;
    }
    
    public String getFaultStatusAsString(){
    	return faultstatus.getFaultStatus();
    }
    
    public FaultStatus getStatus(){
    	return faultstatus;
    }
    
    public FaultStatus getFaultStatusNew(){
    	return faultStatusNew;
    }
    
    public FaultStatus getFaultStatusLogged(){
    	return faultStatusLogged;
    }
    
    public FaultStatus getFaultStatusAssignedToWorkOrder(){
    	return faultStatusAssignedToWorkOrder;
    }
    
    public FaultStatus getFaultStatusVerified(){
    	return faultStatusVerified;
    }
    
    public FaultStatus getFaultStatusDuplicate(){
    	return faultStatusDuplicate;
    }
    
    public FaultStatus getFaultStatusNotVerified(){
    	return faultStatusNotVerified;
    }
    
    public FaultStatus getFaultStatusNotExists(){
    	return faultStatusNotExists;
    }
    
    public FaultStatus getFaultStatusAssignDifferentDepartment(){
    	return faultStatusAssignDifferentDepartment;
    }
    
    public FaultStatus getFaultStatusAlreadResolved(){
    	return faultStatusAlreadyResolved;
    }
    
    
    
    public InjectedFault() {
    	InstantiateFaultStatuses();
    	faultstatus = faultStatusNew;
    }
    
    //---- attempting state pattern INCOMPLETE

    public FAULTTYPE getType() {
        return _faultType;
    }
    
    public void setFaultType(FAULTTYPE faultType) {
        this._faultType = faultType;
    }

    public boolean validLocation() {
        return this._validLocation;
    }

    public void validLocation(boolean validLocation) {
        this._validLocation = validLocation;
    }
}
