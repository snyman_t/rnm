package domain.model.faultlogging;

public enum FAULTTYPE {
        POTHOLE, DRAINAGE, TRAFFICLIGHT, ROADMARKING, ACCIDENT, SIGNAGE
}
