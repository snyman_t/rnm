package domain.model.faultlogging;

import domain.repo.SystemPersistableEntity;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:20 AM
 */
public class FaultLocation extends SystemPersistableEntity {

//    private GeoLocation geoLocation;
    private double longitudeDecimalDegrees;
    private double latitudeDecimalDegrees;
    private double latitudeRadians;
    private double longitudeRadians;
    private int simpleXCoordinate;
    private int simpleYCoordinate;
    private String streetName;
    private String streetSuburb;

    public FaultLocation() {
    }

    public FaultLocation(String streetName, String streetSuburb, int simpleXCoordinate, int simpleYCoordinate) {
        this.streetName = streetName;
        this.streetSuburb = streetSuburb;
        this.simpleXCoordinate = simpleXCoordinate;
        this.simpleYCoordinate = simpleYCoordinate;
    }

    public FaultLocation(String streetName, String streetSuburb) {
        this.streetName = streetName;
        this.streetSuburb = streetSuburb;
    }

    public void initalizeFromSimpleCoOrdinates(int aSimpleXCoordinate, int aSimpleYCoordinate) {
        this.simpleXCoordinate = aSimpleXCoordinate;
        this.simpleYCoordinate = aSimpleYCoordinate;
        //TODO - FUTURE Implementation : this is not compatible with GeoLocation format
    }

    public double getLatitudeRadians() {
        return latitudeRadians;
    }

    public void setLatitudeRadians(double latitudeRadians) {
        this.latitudeRadians = latitudeRadians;
    }

    public double getLongitudeRadians() {
        return longitudeRadians;
    }

    public void setLongitudeRadians(double longitudeRadians) {
        this.longitudeRadians = longitudeRadians;
    }

    public int getSimpleXCoordinate() {
        return simpleXCoordinate;
    }

    public void setSimpleXCoordinate(int simpleXCoordinate) {
        this.simpleXCoordinate = simpleXCoordinate;
    }

    public int getSimpleYCoordinate() {
        return simpleYCoordinate;
    }

    public void setSimpleYCoordinate(int simpleYCoordinate) {
        this.simpleYCoordinate = simpleYCoordinate;
    }

    // Implements an simple Maths distance calculation based on x and y coordinates
    public int getDistanceToCoordinates(int aSimpleXCoordinate, int aSimpleYCoordinate) {
        int x1 = this.simpleXCoordinate;
        int x2 = aSimpleXCoordinate;
        int y1 = this.simpleYCoordinate;
        int y2 = aSimpleYCoordinate;
        return (int) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

//    FUTURE Implementation : Implements an advanced distance calculation based on Geolocation and radians
//    public double getDistanceTo(GeoLocation userLocation, double radians) {
//        return this.geoLocation.distanceTo(userLocation, radians);
//    }

    public double getLongitudeDecimalDegrees() {
        return longitudeDecimalDegrees;
    }

    public void setLongitudeDecimalDegrees(int longitudeDecimalDegrees) {
        this.longitudeDecimalDegrees = longitudeDecimalDegrees;
    }

    public double getLatitudeDecimalDegrees() {
        return latitudeDecimalDegrees;
    }

    public void setLatitudeDecimalDegrees(int latitudeDecimalDegrees) {
        this.latitudeDecimalDegrees = latitudeDecimalDegrees;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetSuburb() {
        return streetSuburb;
    }

    public void setStreetSuburb(String streetSuburb) {
        this.streetSuburb = streetSuburb;
    }
}
