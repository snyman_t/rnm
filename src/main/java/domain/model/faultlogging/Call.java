package domain.model.faultlogging;

import domain.repo.SystemPersistableEntity;

import java.util.Date;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/28 12:44 AM
 */
public class Call extends SystemPersistableEntity {

    public enum CALLSTATUS {
        NEW, DUPLICATE, RECURRANCE
    }

    private String callerReferenceID;
    private CALLSTATUS status;
    private Date auditDate;
    private long linkedFaultID;

    public static CALLSTATUS StringToFaultStatus(String stringStatus) {
        switch (stringStatus) {
            case "NEW":
                return CALLSTATUS.NEW;
            case "DUPLICATE":
                return CALLSTATUS.DUPLICATE;
            case "RECURRANCE":
                return CALLSTATUS.RECURRANCE;
        }
        return null;
    }

    public String getCallerReferenceID() {
        return callerReferenceID;
    }

    public void setCallerReferenceID(String callerReferenceID) {
        this.callerReferenceID = callerReferenceID;
    }

    public CALLSTATUS getStatus() {
        return status;
    }

    public void setStatus(CALLSTATUS status) {
        this.status = status;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public long getLinkedFaultID() {
        return linkedFaultID;
    }

    public void setLinkedFaultID(long linkedFaultID) {
        this.linkedFaultID = linkedFaultID;
    }
}
