package domain.model.faultlogging;

import domain.repo.SystemPersistableEntity;

import java.util.Date;


/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:50 AM
 */
public class Fault extends SystemPersistableEntity implements IFault {

    private long faultID;
    private STATUS status;
    private String description;
    private FaultLocation faultLocation;
    private FAULTTYPE type;
    private PRIORITY priority;
    private Date expectedDateOfResolution;
    private int numberOfQueries;
    private FAULTSOURCE source;

    public long getFaultID() {
        return faultID;
    }

    public void setFaultID(long faultID) {
        this.faultID = faultID;
    }

    public enum STATUS {
        NEW, INVESTIGATING, REPAIRING, CLOSED, DUPLICATE
    }

    public static String faultTypeToString(FAULTTYPE faulttype) {
        switch (faulttype) {
            case POTHOLE:
                return "POTHOLE";
            case DRAINAGE:
                return "DRAINAGE";
            case TRAFFICLIGHT:
                return "TRAFFICLIGHT";
            case ROADMARKING:
                return "ROADMARKING";
            case ACCIDENT:
                return "ACCIDENT";
            case SIGNAGE:
                return "SIGNAGE";
        }
        return null;
    }

    public static FAULTTYPE StringToFaultType(String stringFaultType) {
        switch (stringFaultType) {
            case "POTHOLE":
                return FAULTTYPE.POTHOLE;
            case "DRAINAGE":
                return FAULTTYPE.DRAINAGE;
            case "TRAFFICLIGHT":
                return FAULTTYPE.TRAFFICLIGHT;
            case "ROADMARKING":
                return FAULTTYPE.ROADMARKING;
            case "ACCIDENT":
                return FAULTTYPE.ACCIDENT;
            case "SIGNAGE":
                return FAULTTYPE.SIGNAGE;
        }
        return null;
    }

    public static PRIORITY StringToPriority(String stringPriority) {
        switch (stringPriority) {
            case "NORMAL":
                return PRIORITY.NORMAL;
            case "HIGH":
                return PRIORITY.HIGH;
            case "LOW":
                return PRIORITY.LOW;
        }
        return null;
    }

    public static FAULTSOURCE StringToSource(String stringSource) {
        switch (stringSource) {
            case "CALL_CENTRE":
                return FAULTSOURCE.CALL_CENTRE;
            case "MOBILE_APP":
                return FAULTSOURCE.MOBILE_APP;
            case "SMS":
                return FAULTSOURCE.SMS;
            case "EMAIL":
                return FAULTSOURCE.EMAIL;
        }
        return null;
    }

    public static STATUS StringToFaultStatus(String stringStatus) {
        switch (stringStatus) {
            case "NEW":
                return STATUS.NEW;
            case "INVESTIGATING, , , ":
                return STATUS.INVESTIGATING;
            case "REPAIRING":
                return STATUS.REPAIRING;
            case "CLOSED":
                return STATUS.CLOSED;
            case "DUPLICATE":
                return STATUS.DUPLICATE;
        }
        return null;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FaultLocation getFaultLocation() {
        return faultLocation;
    }

    public void setFaultLocation(FaultLocation faultLocation) {
        this.faultLocation = faultLocation;
    }

    public FAULTTYPE getType() {
        return type;
    }

    public void setType(FAULTTYPE type) {
        this.type = type;
    }

    public PRIORITY getPriority() {
        return priority;
    }

    public void setPriority(PRIORITY priority) {
        this.priority = priority;
    }

    public Date getExpectedDateOfResolution() {
        return expectedDateOfResolution;
    }

    public void setExpectedDateOfResolution(Date expectedDateOfResolution) {
        this.expectedDateOfResolution = expectedDateOfResolution;
    }

    public int getNumberOfQueries() {
        return numberOfQueries;
    }

    public void setNumberOfQueries(int numberOfQueries) {
        this.numberOfQueries = numberOfQueries;
    }

    public FAULTSOURCE getSource() {
        return source;
    }

    public void setSource(FAULTSOURCE source) {
        this.source = source;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("faultID=" + faultID);
        sb.append(";status=" + status);
        sb.append(";description=" + description);
        sb.append(";faultLocation=" + faultLocation);
        sb.append(";type=" + type);
        sb.append(";priority=" + priority);
        sb.append(";expectedDateOfResolution=" + expectedDateOfResolution);
        sb.append(";numberOfQueries=" + numberOfQueries);
        sb.append(";source=" + source);
        return sb.toString();
    }
}
