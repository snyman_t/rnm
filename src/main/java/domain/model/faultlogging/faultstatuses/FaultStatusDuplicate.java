package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusDuplicate extends FaultStatusDefault{
	
	public FaultStatusDuplicate(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "DUPLICATE";
	}
}
