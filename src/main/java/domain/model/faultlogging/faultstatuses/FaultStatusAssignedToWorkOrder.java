package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusAssignedToWorkOrder extends FaultStatusDefault{
	
	public FaultStatusAssignedToWorkOrder(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "ASSIGNED_TO_WORKORDER";
	}
}
