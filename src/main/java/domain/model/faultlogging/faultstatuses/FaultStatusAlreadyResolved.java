package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusAlreadyResolved extends FaultStatusDefault{
	
	public FaultStatusAlreadyResolved(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "ALREADY_RESOLVED";
	}
}
