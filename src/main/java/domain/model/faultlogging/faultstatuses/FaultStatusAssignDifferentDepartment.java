package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusAssignDifferentDepartment extends FaultStatusDefault{
	public FaultStatusAssignDifferentDepartment(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "ASSIGN_DIFFERENT_DEPARTMENT";
	}
}
