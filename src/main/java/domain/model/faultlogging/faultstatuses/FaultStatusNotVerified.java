package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusNotVerified extends FaultStatusDefault{
	
	public FaultStatusNotVerified(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "NOT_VERIFIED";
	}
}
