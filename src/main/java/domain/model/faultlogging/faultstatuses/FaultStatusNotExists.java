package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusNotExists extends FaultStatusDefault{
	
	public FaultStatusNotExists(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "NOT_EXISTS";
	}
	
}
