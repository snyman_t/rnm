package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusVerified extends FaultStatusDefault{
	
	public FaultStatusVerified(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "VERIFIED";
	}
}
