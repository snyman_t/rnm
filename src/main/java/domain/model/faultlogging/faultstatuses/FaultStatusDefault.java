package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.FaultStatus;
import domain.model.faultlogging.InjectedFault;

abstract public class FaultStatusDefault implements FaultStatus{
	InjectedFault injectedFault;
	
	
    abstract public String getFaultStatus();

    public FaultStatusDefault(InjectedFault injectedFault){
    	this.injectedFault = injectedFault;
    }
    
	public void resetStatus(){
		System.out.println("Fault Status reset to NEW");
	}
	
	public void nextStatus(){
		System.out.println("Set fault status to next status");
	}
}
