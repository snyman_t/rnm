package domain.model.faultlogging.faultstatuses;

import domain.model.faultlogging.InjectedFault;

public class FaultStatusLogged extends FaultStatusDefault{

	public FaultStatusLogged(InjectedFault injectedFault){
		super(injectedFault);
	}
	
	public String getFaultStatus(){
		return "LOGGED";
	}
}
