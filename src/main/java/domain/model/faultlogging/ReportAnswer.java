package domain.model.faultlogging;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:15 AM
 */
public class ReportAnswer {

    private String options;
    private String freeText;
    private int numbers;

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }
}
