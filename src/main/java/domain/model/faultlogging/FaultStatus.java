package domain.model.faultlogging;

public interface FaultStatus {
	public void nextStatus();
	public void resetStatus();
	public String getFaultStatus();
}
