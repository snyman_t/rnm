package domain.model.faultlogging;

import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:13 AM
 */
public class FaultReportCard {

    private int reportCardVersion;
    private long reportID;
    private FAULTTYPE faulttype;
    private FaultLocation faultLocation;
    private List<ReportQuestion> questions;
    private List<ReportAnswer> answers;
    private List<ReportImage> images;

    public int getReportCardVersion() {
        return reportCardVersion;
    }

    public void setReportCardVersion(int reportCardVersion) {
        this.reportCardVersion = reportCardVersion;
    }

    public long getReportID() {
        return reportID;
    }

    public void setReportID(long reportID) {
        this.reportID = reportID;
    }

    public FAULTTYPE getFaulttype() {
        return faulttype;
    }

    public void setFaulttype(FAULTTYPE faulttype) {
        this.faulttype = faulttype;
    }

    public List<ReportQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<ReportQuestion> questions) {
        this.questions = questions;
    }

    public List<ReportAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<ReportAnswer> answers) {
        this.answers = answers;
    }

    public List<ReportImage> getImages() {
        return images;
    }

    public void setImages(List<ReportImage> images) {
        this.images = images;
    }
}
