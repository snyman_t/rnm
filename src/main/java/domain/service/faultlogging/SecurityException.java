package domain.service.faultlogging;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/16 5:19 PM
 */
public class SecurityException extends Exception {

    public static String AUTHENTICATION_ERROR = "Unknown Username or Password";
    public static String AUTHORIZATION_ERROR = "User does not have permission to use service";

    public SecurityException() {
        super();
    }

    public SecurityException(String message) {
        super(message);
    }

}
