package domain.service.faultlogging;


import domain.model.faultlogging.Fault;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:58 AM
 */

public class FaultLoggingBusinessService implements IFaultLoggingBusinessService {

    //    private FaultRepository faultRepository;
//    private Repository<Fault> faultRepository = new MockRepository<Fault>();
    private IFaultLoggingRepositoryService repositoryService;

    public FaultLoggingBusinessService(IFaultLoggingRepositoryService faultLoggingRepositoryService) {
        repositoryService = faultLoggingRepositoryService;
    }

//    public List<Fault> getAllOpenFaults(Fault fault) {
//        //return faultRepository.getAllOpenFaults();
//        return faultRepository.getAll();
//    }

//    public List<Fault> getNearbyFaults(String location) {
////        List<Fault> faultList = faultRepository.getAll();
//        List<Fault> faultList = repositoryService.getAllOpenFaults();
//        List<Fault> resultList = new ArrayList<Fault>();
//        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
//            Fault currentFault = iterator.next();
//            if (currentFault.getFaultLocation().getStreetAddress().equals(location)) {
//                resultList.add(currentFault);
//            }
//        }
//        return resultList;
//    }

    /* NOTE :
    This method matches a given street location, to the existing records in the fault repository.
    It is NOT using an optimized pattern matching algorithm, and the intention is to integrate with a
    fully functional address geo-location system.
    Currently matches are included if the street and suburb are the same
     */
    public List<Fault> getNearbyFaultsByStreetAddress(String streetName, String streetSuburb) {
        List<Fault> faultList = repositoryService.getAllOpenFaults();
        List<Fault> resultList = new ArrayList<Fault>();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            // Rule 1 - Add if Suburb and Street is matched
            if ((currentFault.getFaultLocation().getStreetName().equals(streetName)) &&
                    (currentFault.getFaultLocation().getStreetSuburb().equals(streetSuburb))) {
                resultList.add(currentFault);
            }
        }
        return resultList;
    }

    /* NOTE :
    This method matches a given GeoLocation, to the existing records in the fault repository.
    It is NOT using an optimized pattern matching algorithm, and the intention is to integrate with a
    fully functional address geo-location system.
    Currently matches are included distance is less than 50,Unit
     */
    public List<Fault> getNearbyFaultsByGeoLocation(int sourceXCoordinate, int sourceYCoordinate) {
        List<Fault> faultList = repositoryService.getAllOpenFaults();
        List<Fault> resultList = new ArrayList<Fault>();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            // Calculate distance to source using a basic Match algorithm
            int distanceToSource = currentFault.getFaultLocation().getDistanceToCoordinates(sourceXCoordinate, sourceYCoordinate);
            System.out.println(String.format(
                    "Calculating distance between (X1:%d,X2:%d)and(Y1:%d,Y2:%d) = distance to %s-%s-%s is %d ",
                    currentFault.getFaultLocation().getSimpleXCoordinate(),
                    sourceXCoordinate,
                    currentFault.getFaultLocation().getSimpleYCoordinate(),
                    sourceYCoordinate,
                    currentFault.getDescription(),
                    currentFault.getFaultLocation().getStreetName(),
                    currentFault.getFaultLocation().getStreetSuburb(),
                    distanceToSource
            ));
            if (distanceToSource <= FaultService.MAX_DISTANCE_ALLOWED) {
                System.out.println(">>>> Succesful Distance match for "
                        + "faultID=" + currentFault.getFaultID()
                        + "::getDescription()=" + currentFault.getDescription()
                );
                resultList.add(currentFault);
            }
        }
        return resultList;
    }

    public boolean checkNotDuplicateFault(String streetName, String streetSuburb) {
        //return faultRepository.checkNotDuplicateFault(faultAddress);
        boolean resultFlag = false;
        List<Fault> faultList = repositoryService.getAllOpenFaults();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            if ((currentFault.getFaultLocation().getStreetName().equals(streetName)) &&
                    (currentFault.getFaultLocation().getStreetSuburb().equals(streetSuburb))) {
                resultFlag = true;
            }
        }
        return resultFlag;
    }
//    public boolean checkNotDuplicateFault(String faultAddress) {
//        //return faultRepository.checkNotDuplicateFault(faultAddress);
//        boolean resultFlag = false;
//        //List<Fault> faultList = faultRepository.getAll();
//        List<Fault> faultList = faultList = repositoryService.getAllOpenFaults();
//        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
//            Fault currentFault = iterator.next();
//            if (currentFault.getFaultLocation().getStreetAddress().equals(faultAddress)) {
//                resultFlag = true;
//            }
//        }
//        return resultFlag;
//    }

    public Date getExpectedDateOfResolution(int faultID) {
        //Fault resultFault = getFaultDetails(faultID);
        Fault resultFault = repositoryService.getFaultDetails(faultID);
        if (resultFault != null) {
            return resultFault.getExpectedDateOfResolution();
        }
        return null;
    }
}
