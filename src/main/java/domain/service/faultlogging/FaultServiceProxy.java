package domain.service.faultlogging;

import domain.model.faultlogging.FAULTTYPE;
import domain.model.faultlogging.Fault;

import java.util.Date;
import java.util.List;


/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/16 5:16 PM
 */
public class FaultServiceProxy extends BaseSecurityProxy {

    private FaultService faultService;

    public FaultServiceProxy(Profile profile) throws SecurityException {
        super(profile);
        //Perform Security Authentication
        verifySecurityAuthentication(profile);
        faultService = new FaultService();
    }

    public void addNewFault(Fault fault) throws SecurityException {
        //Perform Security Authorization Check
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        faultService.addNewFault(fault);
    }

    public void updateFaultStatus(long faultID, Fault.STATUS newStatus) throws SecurityException {
        //Perform Security Authorization Check
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        faultService.updateFaultStatus(faultID, newStatus);
    }

    public List<Fault> getAllOpenFaults() throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.getAllOpenFaults();
    }

    public List<Fault> getNearbyFaultsByStreetAddress(String streetName, String streetSuburb) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.getNearbyFaultsByStreetAddress(streetName, streetSuburb);
    }

    public List<Fault> getNearbyFaultsByGeoLocation(int sourceXCoordinate, int sourceYCoordinate) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.getNearbyFaultsByGeoLocation(sourceXCoordinate, sourceYCoordinate, FaultService.MAX_DISTANCE_ALLOWED);
    }

    public Fault getFaultDetailsByID(int faultID) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.getFaultDetailsByID(faultID);
    }

    public Fault getFaultDetailsByGeoCode(int xCoOrdinate, int yCoOrdinate) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.getFaultDetailsByGeoCode(xCoOrdinate, yCoOrdinate);
    }

    public boolean checkNotDuplicateFault(String streetName, String streetSuburb) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.checkNotDuplicateFault(streetName, streetSuburb);
    }


    public void updateFaultType(int faultID, FAULTTYPE faulttype) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        faultService.updateFaultType(faultID, faulttype);
    }

    public Date getExpectedDateOfResolution(int faultID) throws SecurityException {
        verifySecurityAuthorization(IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR);
        return faultService.getExpectedDateOfResolution(faultID);
    }

}
