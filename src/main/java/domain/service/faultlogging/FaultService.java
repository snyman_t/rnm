package domain.service.faultlogging;

import domain.model.faultlogging.FAULTTYPE;
import domain.model.faultlogging.Fault;
import domain.repo.MockRepository;
import domain.repo.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:58 AM
 */

public class FaultService {

    // This is used for distance by GeoLocation calculation
    public static final int MAX_DISTANCE_ALLOWED = 50;

    //    private FaultRepository faultRepository;
    private Repository<Fault> faultRepository = new MockRepository<Fault>();
    // NOTE : this will ideally be moved to a sequence generating service in future
    private int sequenceCounter = 1;

    public FaultService() {
    }

    public void addNewFault(Fault fault) {
//        faultRepository.addFault(fault);
        faultRepository.add(fault);
    }

    public void updateFaultStatus(long faultID, Fault.STATUS newStatus) {
//        faultRepository.updateFaultStatus(faultID, newStatus);
        Fault storedFault = faultRepository.findById((int) faultID);
        storedFault.setStatus(newStatus);
        faultRepository.update(storedFault);
    }

    public void updateFaultGeoLocation(long faultID, int newXCoordinate, int newYCoordinate) {
        Fault storedFault = faultRepository.findById((int) faultID);
        storedFault.getFaultLocation().setSimpleXCoordinate(newXCoordinate);
        storedFault.getFaultLocation().setSimpleYCoordinate(newYCoordinate);
        faultRepository.update(storedFault);
    }

    public List<Fault> getAllOpenFaults() {
        //return faultRepository.getAllOpenFaults();
        return faultRepository.getAll();
    }

    /* NOTE :
    This method matches a given street location, to the existing records in the fault repository.
    It is NOT using an optimized pattern matching algorithm, and the intention is to integrate with a
    fully functional address geo-location system.
    Currently matches are included if the street and suburb are the same
     */
    public List<Fault> getNearbyFaultsByStreetAddress(String streetName, String streetSuburb) {
        List<Fault> faultList = faultRepository.getAll();
        List<Fault> resultList = new ArrayList<Fault>();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            // Rule 1 - Add if Suburb and Street is matched
            if ((currentFault.getFaultLocation().getStreetName().equals(streetName)) &&
                    (currentFault.getFaultLocation().getStreetSuburb().equals(streetSuburb))) {
                resultList.add(currentFault);
            }
        }
        return resultList;
    }

    /* NOTE :
    This method matches a given GeoLocation, to the existing records in the fault repository.
    It is NOT using an optimized pattern matching algorithm, and the intention is to integrate with a
    fully functional address geo-location system.
    Currently matches are included distance is less than 50,Unit
     */
    public List<Fault> getNearbyFaultsByGeoLocation(int sourceXCoordinate, int sourceYCoordinate, int maximumUnitsOfDistance) {
        List<Fault> faultList = faultRepository.getAll();
        List<Fault> resultList = new ArrayList<Fault>();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            // Calculate distance to source using a basic Match algorithm
            int distanceToSource = currentFault.getFaultLocation().getDistanceToCoordinates(sourceXCoordinate, sourceYCoordinate);
            System.out.println(String.format(
                    "Calculating distance between (X1:%d,X2:%d)and(Y1:%d,Y2:%d) = distance to %s-%s-%s is %d ",
                    currentFault.getFaultLocation().getSimpleXCoordinate(),
                    sourceXCoordinate,
                    currentFault.getFaultLocation().getSimpleYCoordinate(),
                    sourceYCoordinate,
                    currentFault.getDescription(),
                    currentFault.getFaultLocation().getStreetName(),
                    currentFault.getFaultLocation().getStreetSuburb(),
                    distanceToSource
            ));
            if (distanceToSource <= maximumUnitsOfDistance) {
                System.out.println(">>>> Succesful Distance match for "
                        + "faultID=" + currentFault.getFaultID()
                        + "::getDescription()=" + currentFault.getDescription()
                );
                resultList.add(currentFault);
            }
        }
        return resultList;
    }

    public Fault getFaultDetailsByID(long faultID) {
        return faultRepository.findById((int) faultID);
    }

    public Fault getFaultDetailsByGeoCode(int xCoOrdinate, int yCoOrdinate) {
        List<Fault> faultList = faultRepository.getAll();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            if ((currentFault.getFaultLocation().getSimpleXCoordinate() == xCoOrdinate) &&
                    (currentFault.getFaultLocation().getSimpleYCoordinate() == yCoOrdinate)) {
                return currentFault;
            }
        }
        return null;
//        return faultRepository.getFaultDetailsByAddress(faultAddress);
    }

    public boolean checkNotDuplicateFault(String streetName, String streetSuburb) {
        //return faultRepository.checkNotDuplicateFault(faultAddress);
        boolean resultFlag = false;
        List<Fault> faultList = faultRepository.getAll();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            if ((currentFault.getFaultLocation().getStreetName().equals(streetName)) &&
                    (currentFault.getFaultLocation().getStreetSuburb().equals(streetSuburb))) {
                resultFlag = true;
            }
        }
        return resultFlag;
    }


//    public FaultRepository getFaultRepository() {
//        return faultRepository;
//    }
//
//    public void setFaultRepository(FaultRepository faultRepository) {
//        this.faultRepository = faultRepository;
//    }

    public void updateFaultType(int faultID, FAULTTYPE faulttype) {
        Fault resultFault = getFaultDetailsByID(faultID);
        resultFault.setType(faulttype);
    }

    public Date getExpectedDateOfResolution(int faultID) {
        Fault resultFault = getFaultDetailsByID(faultID);
        if (resultFault != null) {
            return resultFault.getExpectedDateOfResolution();
        }
        return null;
    }

    public long generateNewFaultID() {
        return sequenceCounter++;
    }
}
