package domain.service.faultlogging;


import domain.model.faultlogging.FAULTTYPE;
import domain.model.faultlogging.Fault;
import domain.repo.MockRepository;
import domain.repo.Repository;

import java.util.Iterator;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:58 AM
 */

public class FaultLoggingRepositoryService implements IFaultLoggingRepositoryService {

    //    private FaultRepository faultRepository;
    private Repository<Fault> faultRepository = new MockRepository<Fault>();

    public FaultLoggingRepositoryService() {
    }

    public List<Fault> getAllOpenFaults() {
        //return faultRepository.getAllOpenFaults();
        return faultRepository.getAll();
    }

    public void addNewFault(Fault fault) {
//        faultRepository.addFault(fault);
        faultRepository.add(fault);
    }

    public void updateFaultStatus(long faultID, Fault.STATUS newStatus) {
//        faultRepository.updateFaultStatus(faultID, newStatus);
        Fault storedFault = faultRepository.findById((int) faultID);
        storedFault.setStatus(newStatus);
        faultRepository.update(storedFault);
    }

    public void updateFaultType(int faultID, FAULTTYPE faulttype) {
        Fault storedFault = faultRepository.findById((int) faultID);
        storedFault.setType(faulttype);
        faultRepository.update(storedFault);
    }

    public Fault getFaultDetails(int faultID) {
        return faultRepository.findById(faultID);
    }

    public Fault getFaultDetailsByGeoCode(int xCoOrdinate, int yCoOrdinate) {
        List<Fault> faultList = faultRepository.getAll();
        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
            Fault currentFault = iterator.next();
            if ((currentFault.getFaultLocation().getSimpleXCoordinate() == xCoOrdinate) &&
                    (currentFault.getFaultLocation().getSimpleYCoordinate() == yCoOrdinate)) {
                return currentFault;
            }
        }
        return null;
    }
//    public Fault getFaultDetails(String faultAddress) {
//        List<Fault> faultList = faultRepository.getAll();
//        for (Iterator<Fault> iterator = faultList.iterator(); iterator.hasNext();) {
//            Fault currentFault = iterator.next();
//            if (currentFault.getFaultLocation().getStreetAddress().equals(faultAddress)) {
//                return currentFault;
//            }
//        }
//        return null;
////        return faultRepository.getFaultDetailsByAddress(faultAddress);
//    }

}
