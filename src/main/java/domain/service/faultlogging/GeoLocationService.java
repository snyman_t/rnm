package domain.service.faultlogging;

import domain.model.faultlogging.FaultLocation;
import domain.repo.MockRepository;
import domain.repo.Repository;

import java.util.Iterator;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/21 11:32 PM
 */
public class GeoLocationService {

    private Repository<FaultLocation> geolocationRepository = new MockRepository<FaultLocation>();

    public void add(FaultLocation location) {
        geolocationRepository.add(location);
    }

    public FaultLocation searchByCoordinate(int simpleXCoordinate, int simpleYCoordinate) {
        List<FaultLocation> geoLocationList = geolocationRepository.getAll();
        for (Iterator<FaultLocation> iterator = geoLocationList.iterator(); iterator.hasNext();) {
            FaultLocation listItem = iterator.next();
            if (listItem.getSimpleXCoordinate() == simpleXCoordinate && listItem.getSimpleYCoordinate() == simpleYCoordinate) {
                return listItem;
            }
        }
        return null;
    }

    public FaultLocation searchByStreetAddress(String streetName, String streetSuburb) {
        List<FaultLocation> geoLocationList = geolocationRepository.getAll();
        for (Iterator<FaultLocation> iterator = geoLocationList.iterator(); iterator.hasNext();) {
            FaultLocation listItem = iterator.next();
            if (listItem.getStreetName().equals(streetName) && listItem.getStreetSuburb().equals(streetSuburb)) {
                return listItem;
            }
        }
        return null;
    }
}
