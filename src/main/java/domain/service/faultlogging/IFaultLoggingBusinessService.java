package domain.service.faultlogging;

import domain.model.faultlogging.Fault;

import java.util.Date;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version shameer - 2015/06/21 1:34 PM
 */
public interface IFaultLoggingBusinessService {
//    List<Fault> getAllOpenFaults(Fault fault);

//    List<Fault> getNearbyFaults(String location);
    List<Fault> getNearbyFaultsByStreetAddress(String streetName, String streetSuburb);

    List<Fault> getNearbyFaultsByGeoLocation(int sourceXCoordinate, int sourceYCoordinate);

    boolean checkNotDuplicateFault(String streetName, String streetSuburb);

//    boolean checkNotDuplicateFault(String faultAddress);

    Date getExpectedDateOfResolution(int faultID);
}
