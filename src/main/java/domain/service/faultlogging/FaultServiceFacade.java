package domain.service.faultlogging;


import domain.model.faultlogging.FAULTTYPE;
import domain.model.faultlogging.Fault;

import java.util.Date;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version shameer - 2015/06/10 1:41 PM
 */
public class FaultServiceFacade implements IFaultLoggingRepositoryService, IFaultLoggingBusinessService {

    private IFaultLoggingRepositoryService repositoryService;
    private IFaultLoggingBusinessService businessService;


    public FaultServiceFacade(IFaultLoggingRepositoryService aRepositoryService, IFaultLoggingBusinessService aBusinessService) {
        this.repositoryService = aRepositoryService;
        this.businessService = aBusinessService;
    }

//    public List<Fault> getNearbyFaults(String location) {
//        return businessService.getNearbyFaults(location);
//    }

//    public boolean checkNotDuplicateFault(String faultAddress) {
//        return businessService.checkNotDuplicateFault(faultAddress);
//    }

    public List<Fault> getNearbyFaultsByStreetAddress(String streetName, String streetSuburb) {
        return businessService.getNearbyFaultsByStreetAddress(streetName, streetSuburb);
    }

    public List<Fault> getNearbyFaultsByGeoLocation(int sourceXCoordinate, int sourceYCoordinate) {
        return businessService.getNearbyFaultsByGeoLocation(sourceXCoordinate, sourceYCoordinate);
    }

    public boolean checkNotDuplicateFault(String streetName, String streetSuburb) {
        return businessService.checkNotDuplicateFault(streetName, streetSuburb);
    }

    public Date getExpectedDateOfResolution(int faultID) {
        return businessService.getExpectedDateOfResolution(faultID);
    }

    public List<Fault> getAllOpenFaults() {
        return repositoryService.getAllOpenFaults();
    }

    public void addNewFault(Fault fault) {
        repositoryService.addNewFault(fault);
    }

    public void updateFaultStatus(long faultID, Fault.STATUS newStatus) {
        repositoryService.updateFaultStatus(faultID, newStatus);
    }

    public void updateFaultType(int faultID, FAULTTYPE faulttype) {
        repositoryService.updateFaultType(faultID, faulttype);
    }

    public Fault getFaultDetails(int faultID) {
        return repositoryService.getFaultDetails(faultID);
    }

    public Fault getFaultDetailsByGeoCode(int xCoOrdinate, int yCoOrdinate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

//    public Fault getFaultDetails(String faultAddress) {
//        return repositoryService.getFaultDetails(faultAddress);
//    }
}
