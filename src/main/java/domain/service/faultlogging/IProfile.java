package domain.service.faultlogging;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/16 4:21 PM
 */
public interface IProfile {

    public enum PROFILE_ROLE {
        CALL_CENTER_OPERATOR,
        FAULT_INVESTIGATOR,
        WORK_ORDER_CREATOR,
        WORK_ORDER_DISPATCHER,
        REPAIR_TEAM_LEADER,
        SYSTEM_USER,
        FAULT_SUPERVISOR,
        FAULT_INSPECTOR
    }
}
