package domain.service.faultlogging;

import domain.model.faultlogging.FAULTTYPE;
import domain.model.faultlogging.Fault;

import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version shameer - 2015/06/21 1:33 PM
 */
public interface IFaultLoggingRepositoryService {

    List<Fault> getAllOpenFaults();

    void addNewFault(Fault fault);

    void updateFaultStatus(long faultID, Fault.STATUS newStatus);

    void updateFaultType(int faultID, FAULTTYPE faulttype);

    Fault getFaultDetails(int faultID);

    Fault getFaultDetailsByGeoCode(int xCoOrdinate, int yCoOrdinate);
//    Fault getFaultDetails(String faultAddress);
}
