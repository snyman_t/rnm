package domain.service.faultlogging;



import java.util.Iterator;
import java.util.List;

import domain.repo.Repository;
import domain.repo.RepositoryFactory;
import domain.repo.SystemPersistableEntity;


/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/16 6:11 PM
 */
public class BaseSecurityProxy {
    protected Repository<Profile> profileRepository;
    protected Profile profile;

    public Profile loginUser(String username, String password) throws SecurityException {
        profileRepository = RepositoryFactory.getFactoryInstance().getProfileRepository();
        List<Profile> profileList = profileRepository.getAll();
        for (Iterator<Profile> iterator = profileList.iterator(); iterator.hasNext();) {
            Profile listElement = iterator.next();
            if (listElement.getUsername().equals(username)) {
                return listElement;
            }
        }
        //If no username matched then throw Authentication Security Error
        throw new SecurityException(SecurityException.AUTHENTICATION_ERROR);
    }

    // Constructor - with username and password
    public BaseSecurityProxy(String username, String password) throws SecurityException {
        profileRepository = RepositoryFactory.getFactoryInstance().getProfileRepository();
        this.profile = loginUser(username,password);
    }

    // Constructor - with Profile
    public BaseSecurityProxy(Profile profile) {
        profileRepository = RepositoryFactory.getFactoryInstance().getProfileRepository();
        this.profile = profile;
    }

    protected void verifySecurityAuthorization(IProfile.PROFILE_ROLE role) throws SecurityException {
        if (!profile.getProfileRole().equals(role)) {
            throw new SecurityException(SecurityException.AUTHORIZATION_ERROR);
        }

    }

    protected void verifySecurityAuthentication(Profile profile) throws SecurityException {
        //Perform Security Authentication
        if (profileRepository.findById(profile.getId()) == null) {
            throw new SecurityException(SecurityException.AUTHENTICATION_ERROR);
        }
    }
}
