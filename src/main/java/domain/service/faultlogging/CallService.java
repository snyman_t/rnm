package domain.service.faultlogging;

import domain.model.faultlogging.Call;
import domain.repo.MockRepository;
import domain.repo.Repository;

import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:58 AM
 */

public class CallService {

    //    private FaultRepository faultRepository;
    private Repository<Call> callRepository = new MockRepository<Call>();
    // NOTE : this will ideally be moved to a sequence generating service in future
    private int sequenceCounter = 1;

    public CallService() {
    }

    public void addNewCall(Call call) {
//        faultRepository.addFault(fault);
        callRepository.add(call);
    }

    public void updateCallStatus(long callID, Call.CALLSTATUS newStatus) {
//        faultRepository.updateFaultStatus(faultID, newStatus);
        Call storedCall = callRepository.findById((int) callID);
        storedCall.setStatus(newStatus);
        callRepository.update(storedCall);
    }

    public List<Call> getAllCalls() {
        //return faultRepository.getAllOpenFaults();
        return callRepository.getAll();
    }

    public Call getCallDetailsByID(long callID) {
        return callRepository.findById((int) callID);
    }


    public long generateNewCallID() {
        return sequenceCounter++;
    }

    public String generateNewCallReferenceID(int callID) {
        String str = String.valueOf(callID);
        String str2 = "REF" + String.format("%10s", str).replace(' ', '0');
        return str2;
    }
}
