package domain.service.faultlogging;

import domain.repo.SystemPersistableEntity;


/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/16 4:22 PM
 */
public class Profile extends SystemPersistableEntity {

    private int profileID;
    private String username;
    private String password;
    private IProfile.PROFILE_ROLE profileRole;

    public Profile(int profileID, String username, String password, IProfile.PROFILE_ROLE profileRole) {
        this.profileID = profileID;
        this.username = username;
        this.password = password;
        this.profileRole = profileRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public IProfile.PROFILE_ROLE getProfileRole() {
        return profileRole;
    }

    public void setProfileRole(IProfile.PROFILE_ROLE profileRole) {
        this.profileRole = profileRole;
    }

    @Override
    public int getId() {
        return profileID;
    }
}
