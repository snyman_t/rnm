package domain.service.faultverification;

import domain.model.faultlogging.*;

import java.util.List;

import domain.repo.ReportCardRepository;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 9:26 AM
 */
public class FaultVerificationService {

    private ReportCardRepository reportCardRepository;

    List<Fault> GetFaultsByPriorty(PRIORITY priority, int distance) {
        throw new RuntimeException("Not Implemented");
    }

    List<Fault> GetNearbyFault(FaultLocation location) {
        throw new RuntimeException("Not Implemented");

    }

    List<FaultDirection> GetRoute(FaultLocation targetLocation, FaultLocation currentLocation) {
        throw new RuntimeException("Not Implemented");
    }

    List<ReportQuestion> getGetQuestions(FAULTTYPE faulttype) {
        throw new RuntimeException("Not Implemented");
    }

    public void CreateReportCard(List<ReportQuestion> questions, FAULTTYPE faultType) {
        FaultReportCard newFaultReportCard = new FaultReportCard();
        newFaultReportCard.setQuestions(questions);
        newFaultReportCard.setFaulttype(faultType);
        getReportCardRepository().addNewReportCard(newFaultReportCard);
    }

    public void updateReportCardAnswers(int reportID, List<ReportAnswer> reportAnswers) {
        getReportCardRepository().updateReportCardAnswers(reportID, reportAnswers);
    }

    public void updateReportCardImages(int reportID, List<ReportImage> reportImages) {
        getReportCardRepository().updateReportCardImages(reportID, reportImages);
    }


    public ReportCardRepository getReportCardRepository() {
        return reportCardRepository;
    }

    public void setReportCardRepository(ReportCardRepository reportCardRepository) {
        this.reportCardRepository = reportCardRepository;
    }
}
