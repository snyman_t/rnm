package domain.service.faultrepair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import domain.repo.AvailabilityFilter;
import domain.repo.FilteredRepairTeamRepository;
import domain.repo.FilteredRepository;
import domain.repo.RepositoryFactory;
import domain.repo.Repository;
import domain.model.faultrepair.RepairTeam;
import domain.model.faultrepair.WorkOrder;
import domain.model.faultrepair.CalendarEntry;



public class RepairTeamService {
	
	WorkOrderService workOrderService;
	FaultService faultService;
	
	private Repository<RepairTeam> repairTeamRepository; 

	public RepairTeamService(Repository<RepairTeam> repairTeamRepository) {
		this.repairTeamRepository = repairTeamRepository;
	}
	
	public void injectWorkOrderServiceDependancy(WorkOrderService workOrderService){
		this.workOrderService = workOrderService;
	}
	
	public void injectFaultServiceDependancy(FaultService faultService){
		this.faultService = faultService;
	}
	
	

	public void addARepairTeam(String repairTeamName){
		RepairTeam rt = new RepairTeam(repairTeamName);
		
		repairTeamRepository.add(rt);
	}
	
	public int getRepairTeamId(String repairTeamName) {
		
		for(RepairTeam rt : repairTeamRepository.getAll()){
			if(rt.getName() == repairTeamName){
				return rt.getId();
			}
		}
		
		return 0;
		
	}
	
	public String getRepairTeamName(Integer id) {

		
		for(RepairTeam rt : repairTeamRepository.getAll()){
			if(rt.getId() == id){
				return rt.getName();
			}
		}
		
		return null;
		
	}
	
	public List<Integer> getListOfAllRepairTeamIds(){
		
		List<Integer> list = new ArrayList<>();
		
		for(RepairTeam rt : repairTeamRepository.getAll()){
		
			list.add(rt.getId());
		}
		
		return list;
	}
	
	

	public boolean scheduleDateTimeForRepairTeamId(int repairTeamId, int workOrderId, String dateTimeString, int duration) {
		
		RepairTeam rt = repairTeamRepository.findById(repairTeamId);
				
		return  rt.getCalendar().addEntry(workOrderId,dateTimeString,duration);
		
	}
	
	public String getEarliestAvailableSlotForRepairTeamId(int repairTeamId,String currentDateTime, int duration) {
		
		RepairTeam rt = repairTeamRepository.findById(repairTeamId);
		
		
		return rt.getCalendar().earliestOpening(currentDateTime,duration);
		
	}

	public String getScheduledDateTimeForWorkOrder(int workOrderId) {

		int repairTeamId = this.workOrderService.getAssignedRepairTeamIdForWorkOrder(workOrderId);
		
		RepairTeam rt = repairTeamRepository.findById(repairTeamId);
		
		return rt.getCalendar().getScheduledDateTimeForWorkOrderId(workOrderId);

	}
	
	public int getScheduledDurationForWorkOrder(int workOrderId) {

		int repairTeamId = this.workOrderService.getAssignedRepairTeamIdForWorkOrder(workOrderId);
		
		RepairTeam rt = repairTeamRepository.findById(repairTeamId);
		
		return rt.getCalendar().getScheduledDurationForWorkOrderId(workOrderId);

	}
	
	public List<Integer> getRepairTeamIdsAvailableAtDateTime(String startDateTime, int duration) throws ParseException{
		
		List<Integer> repairTeamIds = new ArrayList<>();
		AvailabilityFilter filter = new AvailabilityFilter(startDateTime, duration);
		Repository<RepairTeam> filteredRepo  = new FilteredRepairTeamRepository(repairTeamRepository, filter); 
	
		for(RepairTeam rt : filteredRepo.getAll()){
			repairTeamIds.add(rt.getId());
		}
		
		return repairTeamIds;
	}

	public void delayWorkOrdersForRepairTeam(int repairTeamId, int workOrderId,int hourCount){
		RepairTeam rt = repairTeamRepository.findById(repairTeamId);
		
		rt.getCalendar().delayWorkOrderEntry(workOrderId,hourCount);
		
	}


	
	
	
}
