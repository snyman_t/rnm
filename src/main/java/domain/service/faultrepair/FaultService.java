
package domain.service.faultrepair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultlogging.PRIORITY;
import domain.model.faultrepair.*;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;

public class FaultService{
	
	//Repositories
	Repository<Fault> _faultRepository;
	
	public FaultService(){
		
		//workOrderFactory = new WorkOrderFactory(RepositoryFactory.getFactoryInstance());
		_faultRepository = RepositoryFactory.getFactoryInstance().getFaultRepository();;
	}
	
	public List<Fault> getAllIssuedFaults() {
		return _faultRepository.getAll();
	}
	
	public String getMapOfIssuedFaultsWithPriority() {
		String mapString = "map";
		
		List<Fault> faultlist = _faultRepository.getAll();
		
		for(Fault element : faultlist){
			mapString+="/"+String.valueOf(element.getFaultId())+"-"+element.getFaultPriorityAsString();
		}

		return mapString;
	}
	
	public String getReportCardForFaultId(int faultId) {
		List<Fault> faultlist = _faultRepository.getAll();
		for(Fault element : faultlist){
			if (element.getFaultId() == faultId){
				return element.getReportCard();
			}
		}
		
		return "nomatchfound";
	}
	
	public Fault getFaultForFaultId(int faultId) {
		List<Fault> faultlist = _faultRepository.getAll();
		for(Fault element : faultlist){
			if (element.getFaultId() == faultId){
				return element;
			}
		}
		
		return null;
	}
	
	public void addFault(int faultId,String faultType,String priority,String reportCard){
		FAULTTYPE faultTypeAsEnum = FAULTTYPE.valueOf(faultType.toUpperCase(Locale.ENGLISH));
		PRIORITY prioirityAsEnum = PRIORITY.valueOf(priority.toUpperCase(Locale.ENGLISH));
		
		Fault newFault = new Fault(faultId,faultTypeAsEnum);
		newFault.setPriority(prioirityAsEnum);
		newFault.setReportCard(reportCard);
		_faultRepository.add(newFault);
	}
}
