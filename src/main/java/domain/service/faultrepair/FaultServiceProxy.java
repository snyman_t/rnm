package domain.service.faultrepair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultlogging.PRIORITY;
import domain.model.faultrepair.*;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;
import domain.service.faultlogging.BaseSecurityProxy;
import domain.service.faultlogging.IProfile;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.SecurityException;

public class FaultServiceProxy extends BaseSecurityProxy{

	FaultService _faultservice;
	
	public FaultServiceProxy(Profile profile) throws SecurityException{
		super(profile);
		verifySecurityAuthentication(profile);
		
		_faultservice = new FaultService();
	}
	
	public List<Fault> getAllIssuedFaults() throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);
		//----
		
		return _faultservice.getAllIssuedFaults();
	}
	
	public String getMapOfIssuedFaultsWithPriority() throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);

		return _faultservice.getMapOfIssuedFaultsWithPriority();
	}
	
	public String getReportCardForFaultId(int faultId) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);

		return _faultservice.getReportCardForFaultId(faultId);
	}
	
	public Fault getFaultForFaultId(int faultId) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);

		return _faultservice.getFaultForFaultId(faultId);
	}
	
	public void addFault(int faultId,String faultType,String priority,String reportCard) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);

		_faultservice.addFault(faultId,faultType,priority,reportCard);
	}
}
