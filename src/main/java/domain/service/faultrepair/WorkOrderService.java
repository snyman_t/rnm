package domain.service.faultrepair;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import domain.factory.faultrepair.WorkOrderFactory;
import domain.model.faultlogging.PRIORITY;
import domain.model.faultrepair.BillOfEquipment;
import domain.model.faultrepair.BillOfMaterials;
import domain.model.faultrepair.FAULTTYPE;
import domain.model.faultrepair.Fault;
import domain.model.faultrepair.RepairTaskList;
import domain.model.faultrepair.WorkOrder;
import domain.model.faultrepair.generators.BOEGenerator;
import domain.model.faultrepair.generators.BOMGenerator;
import domain.model.faultrepair.generators.RepairTaskListGenerator;
import domain.repo.FilteredWorkOrderRepository;
import domain.repo.RepositoryFactory;
import domain.repo.Repository;
import domain.repo.WorkOrderFilter;

public class WorkOrderService {
	
	WorkOrderFactory workOrderFactory;
	//FaultFactory faultFactory;
	//RepairTeamFactory repairTeamFactory;
	
	RepairTeamService repairTeamService;
	FaultServiceProxy faultService;
	
	//Repositories
	Repository<WorkOrder> _workOrderRepository;
	Repository<Fault> _faultRepository;
	
	//List  Generators
	BOMGenerator _billOfMaterialsGen;
	BOEGenerator _billOfEquipmentGen;
	RepairTaskListGenerator _taskListGen;
	
	//WorkOrder Components
	RepairTaskList _taskList;
	BillOfMaterials _bom;
	BillOfEquipment _boe;

	public WorkOrderService(){
		
		//workOrderFactory = new WorkOrderFactory(RepositoryFactory.getFactoryInstance());
		
		_billOfEquipmentGen = new BOEGenerator();
		_billOfMaterialsGen = new BOMGenerator();
		_taskListGen = new RepairTaskListGenerator();
		_workOrderRepository = RepositoryFactory.getFactoryInstance().getWorkOrderRepository();
		_faultRepository = RepositoryFactory.getFactoryInstance().getFaultRepository();
		
	}
	
	public WorkOrderService(Repository<WorkOrder> workOrderRepository) {
		this._workOrderRepository = workOrderRepository;
	}

	public void injectRepairTeamServiceDependancy(RepairTeamService repairTeamService){
		this.repairTeamService = repairTeamService;
	}
	
	public void injectFaultServiceDependancy(FaultServiceProxy faultService){
		this.faultService = faultService;
	}
	

	public int getWorkOrderIDForFaultID(int faultID){
		List <WorkOrder> workOrderList = _workOrderRepository.getAll();
		
		int workorderId=0;
		
		for (WorkOrder element: workOrderList){
			if (faultID == element.getFaultID()){
				workorderId=element.getId();
			} 
		}
		
		return workorderId;
	}
	
	public int generateWorkOrderForFaultIDAndTypeAsString(int FaultID, String faultTypeAsString,String faultPriorityAsString){
		
		FAULTTYPE faultTypeAsEnum = FAULTTYPE.valueOf(faultTypeAsString.toUpperCase(Locale.ENGLISH));
		PRIORITY priorityAsEnum = PRIORITY.valueOf(faultPriorityAsString.toUpperCase(Locale.ENGLISH));
		
		Fault newFault = new Fault(FaultID,faultTypeAsEnum);
		newFault.setPriority(priorityAsEnum);
		
		WorkOrder newWorkOrder = generateWorkOrderForFault(newFault);
		return newWorkOrder.getId();
	}
	
	public WorkOrder generateWorkOrderForFault(Fault fault){
		if (fault.getStatus() == fault.getFaultStatusAssignedToWorkOrder()){return null;}
		if (fault.getStatus() == fault.getFaultStatusModificationInProgress()){return null;}
		
		_taskList = _taskListGen.generateTaskListForFaultType(fault.getType());
		_bom = _billOfMaterialsGen.generateBillOfMaterialsForFaultType(fault.getType());
		_boe = _billOfEquipmentGen.generateBillOfEquipmentForFaultType(fault.getType());
		
		WorkOrder workOrder= new WorkOrder(fault);
		
		workOrder.assignBillOfMaterials(_bom);
		workOrder.assignBillOfEquipment(_boe);
		workOrder.assignTaskList(_taskList);
		
		workOrder.setStatus(workOrder.getWorkOrderStatusIssued());
		fault.setStatus(fault.getFaultStatusAssignedToWorkOrder());
		_workOrderRepository.add(workOrder);
			
		return workOrder;
	}
	
	public List<Integer> getAllWorkOrderIds(String status,String priority, String faultType){
		
		List<WorkOrder> workOrderList;
		List<Integer> workOrderIdList = new ArrayList<>();
		
		
		WorkOrderFilter filter = new WorkOrderFilter(FAULTTYPE.valueOf(faultType.toUpperCase(Locale.ENGLISH)), 
												   PRIORITY.valueOf(priority.toUpperCase(Locale.ENGLISH)));
		
		FilteredWorkOrderRepository filteredRepository  = new FilteredWorkOrderRepository(_workOrderRepository, filter);
		
		workOrderList = filteredRepository.getAll();
		
		for(WorkOrder wo : workOrderList){
			workOrderIdList.add(wo.getId());
		}
		
		return workOrderIdList;
		
		
	}
	
	public List<WorkOrder> getAllIssuedWorkOrder(){
		List<WorkOrder>  list = new ArrayList<WorkOrder>();
		return list;
	}
	
	public List<WorkOrder> getAllAssigndWorkOrders(){
		List<WorkOrder>  list = new ArrayList<WorkOrder>();
		return list;
	}
	
	public List<WorkOrder> getAllScheduledWorkOrders(){
		List<WorkOrder>  list = new ArrayList<WorkOrder>();
		return list;
	}
	
	public void assignRepairTeamToWorkOrder(int workOrderId, int repairTeamId){
		WorkOrder wo =  _workOrderRepository.findById(workOrderId);				
		wo.assignRepairTeam(repairTeamId);
		
	}
	
	public boolean scheduleDateTimeForAssignedWorkOrder(int workOrderId,String dateTimeString, int duration) throws ParseException{
		WorkOrder wo = _workOrderRepository.findById(workOrderId);
		
		int repairTeamId = wo.getAssignRepairTeamId();
		
		repairTeamService.scheduleDateTimeForRepairTeamId(repairTeamId,workOrderId,dateTimeString,duration);
		
		wo.setStatus(wo.getWorkOrderStatusScheduled());
		
		return true;

		
	}
	
	public boolean scheduleDateTimeAutomaticallyForIssuedWorkOrder(int workOrderId,String currentDateTime,int duration) throws ParseException{
		
		int repairTeamId = this.getAssignedRepairTeamIdForWorkOrder(workOrderId);
		
		String earliestDateTime = repairTeamService.getEarliestAvailableSlotForRepairTeamId(repairTeamId,currentDateTime,duration);
		
		this.scheduleDateTimeForAssignedWorkOrder(workOrderId,earliestDateTime,duration);
		
		return true;
	}
	
	public boolean delayWorkOrderByHours(int workOrderId,int hourCount){
		WorkOrder wo = _workOrderRepository.findById(workOrderId);
		int repairTeamId = wo.getAssignRepairTeamId();
		
		repairTeamService.delayWorkOrdersForRepairTeam(repairTeamId,workOrderId,hourCount);
		return true;
	}

	public int getAssignedRepairTeamIdForWorkOrder(int workOrderId) {
		
		WorkOrder wo = _workOrderRepository.findById(workOrderId);
		
		return wo.getAssignRepairTeamId();
	}

	public String getWorkOrderStatus(int workOrderId) {
		WorkOrder wo = _workOrderRepository.findById(workOrderId);
		return wo.getStatusAsString();
	}


}
