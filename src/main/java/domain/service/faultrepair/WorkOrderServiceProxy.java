package domain.service.faultrepair;

import java.text.ParseException;
import java.util.List;
import domain.model.faultrepair.Fault;
import domain.model.faultrepair.WorkOrder;
import domain.service.faultlogging.BaseSecurityProxy;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.SecurityException;
import domain.service.faultlogging.IProfile;

public class WorkOrderServiceProxy extends BaseSecurityProxy {
	private WorkOrderService workOrderService;

	public WorkOrderServiceProxy(Profile profile) throws SecurityException{
		super(profile);
		verifySecurityAuthentication(profile);
		
		workOrderService = new WorkOrderService();
	}
	
	public void injectRepairTeamServiceDependancy(RepairTeamService repairTeamService) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);
		//----
		
		workOrderService.injectRepairTeamServiceDependancy(repairTeamService);
	}
	
	public void injectFaultServiceDependancy(FaultServiceProxy faultService) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);
		//----
				
		workOrderService.injectFaultServiceDependancy(faultService);
	}
	
	public int getWorkOrderIDForFaultID(int faultID) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);
		
		return workOrderService.getWorkOrderIDForFaultID(faultID);
	}
	
	public int generateWorkOrderForFaultIDAndTypeAsString(int FaultID, String faultTypeAsString,String faultPriorityAsString) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);
		
		return workOrderService.generateWorkOrderForFaultIDAndTypeAsString(FaultID,faultTypeAsString,faultTypeAsString);
	}
	
	public WorkOrder generateWorkOrderForFault(Fault fault) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR);
		
		return workOrderService.generateWorkOrderForFault(fault);
	}
	
	public List<WorkOrder> getAllIssuedWorkOrder() throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		
		return workOrderService.getAllIssuedWorkOrder();
	}
	
	public List<WorkOrder> getAllAssigndWorkOrders() throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		
		return workOrderService.getAllAssigndWorkOrders();
	}
	
	public List<WorkOrder> getAllScheduledWorkOrders() throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		
		return workOrderService.getAllScheduledWorkOrders();
	}
	
	public void assignRepairTeamToWorkOrder(int workOrderId, int repairTeamId) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);

		workOrderService.assignRepairTeamToWorkOrder(workOrderId,repairTeamId);
	}
	
	public boolean scheduleDateTimeForAssignedWorkOrder(String dateTimeString) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		
		boolean success = true;
		return success;
	}
	
	public boolean scheduleDateTimeAutomaticallyForIssuedWorkOrder(int workOrderId,String currentDateTime,int duration) throws ParseException,SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		
		return workOrderService.scheduleDateTimeAutomaticallyForIssuedWorkOrder(workOrderId,currentDateTime,duration);
	}
	
	public boolean delayWorkOrderByHours(int WorkOrderId,int hourCount) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		//----
		
		boolean success = true;
		return success;
	}

	public int getAssignedRepairTeamIdForWorkOrder(int workOrderId) throws SecurityException {
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		//----
		return 0;
	}

	public String getWorkOrderStatus(int workOrderId) throws SecurityException{
		//authorize service access via proxy pattern 
		verifySecurityAuthorization(IProfile.PROFILE_ROLE.WORK_ORDER_DISPATCHER);
		
		return workOrderService.getWorkOrderStatus(workOrderId);
	}
	

}
