package domain.repo;


import domain.model.faultlogging.FaultReportCard;
import domain.model.faultlogging.ReportAnswer;
import domain.model.faultlogging.ReportImage;

import java.util.Hashtable;
import java.util.List;

/**
 * Version History
 * 1.0 : Initial Version SHAMEER - 2015/06/07 8:59 AM
 */
public class ReportCardRepository {

    protected Hashtable<Long, FaultReportCard> _hashTable;
    private long currentReportCardID;

    public ReportCardRepository() {
        _hashTable = new Hashtable<Long, FaultReportCard>();
        currentReportCardID = 0;
    }

    protected void generateHashtableDummyData() {
        throw new RuntimeException("Not Implemented");
    }

    public void addNewReportCard(FaultReportCard faultReportCard) {

        long nextID = getCurrentReportCardID() + 1;
        faultReportCard.setReportID(nextID);
        setCurrentReportCardID(nextID);
        _hashTable.put(nextID, faultReportCard);
    }

    public void updateReportCardAnswers(int reportID, List<ReportAnswer> reportAnswers) {
        throw new RuntimeException("Not Implemented");
    }

    public void updateReportCardImages(int reportID, List<ReportImage> reportImages) {
        throw new RuntimeException("Not Implemented");
    }

    public FaultReportCard getReportCardByID(long reportCardID) {
        return _hashTable.get(reportCardID);
    }

    public long getCurrentReportCardID() {
        return currentReportCardID;
    }

    public void setCurrentReportCardID(long currentReportCardID) {
        this.currentReportCardID = currentReportCardID;
    }

    public int getRepositorySize() {
        return _hashTable.size();
    }
}
