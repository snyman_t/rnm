package domain.repo;

abstract public class SystemPersistableEntity {
	protected int _id;
	
	public int getId(){
		return this._id;
	}
	
	public void setId(int id){
		 _id = id;
	}
}
