package domain.repo;

import java.util.ArrayList;
import java.util.List;

public class MockRepository<T extends SystemPersistableEntity> implements Repository<T>{

	List<T> entries;
	
	public MockRepository()
	{
		entries = new ArrayList<T>();
	}
	
	@Override
	public void add(T obj) {
		obj.setId(entries.size()+1);
		entries.add(obj);
	}

	@Override
	public void update(T obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		
	}

	@Override
	public T findById(int id) {
		
		for(T entry : entries){
			if(entry.getId() == id){
				return entry;
			}
		}
		
		return null;
		
	}

	@Override
	public List<T> getAll() {
		return entries;
	}

}
