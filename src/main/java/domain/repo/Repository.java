package domain.repo;

import java.util.List;



public interface Repository<T extends SystemPersistableEntity> {
	
	public void add(T obj);
	
	public void update(T obj);
	
	public void delete(int id);
	
	public T findById(int id);
	
	public List<T> getAll();

}
