package domain.repo;

import domain.model.faultlogging.PRIORITY;
import domain.model.faultrepair.FAULTTYPE;

public class WorkOrderFilter {

	FAULTTYPE faultType;
	private PRIORITY faultPriority;
	
	public WorkOrderFilter(FAULTTYPE faultType,PRIORITY faultPriority){
		this.faultType = faultType;
		this.faultPriority = faultPriority;
	}
	
	public FAULTTYPE getfaultType(){
		return faultType;
	}

	public PRIORITY getFaultPriority() {
		return faultPriority;
	}

	
	
}
