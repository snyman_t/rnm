package domain.repo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import domain.model.faultrepair.RepairTeam;
import domain.model.faultrepair.WorkOrder;

public class FilteredRepairTeamRepository extends FilteredRepository<RepairTeam>{

	private Repository<RepairTeam> repo;
	private AvailabilityFilter filter;
	
	public FilteredRepairTeamRepository(Repository<RepairTeam> repo,AvailabilityFilter filter){
		this.repo = repo;
		this.filter= filter;
		
	}
	
	
	public List<RepairTeam> getAll(){

		List<RepairTeam> filteredList = new ArrayList<RepairTeam>();
		
		for(RepairTeam rt : repo.getAll()){
			if(rt.getCalendar().available(filter.getStartDateTime(),filter.getDuration())){
				filteredList.add(rt);
			}
		}
		
		return filteredList;
		
	}
	
}
