package domain.repo;

import domain.model.faultrepair.Fault;
import domain.model.faultrepair.RepairTeam;
import domain.model.faultrepair.WorkOrder;
import domain.service.faultlogging.Profile;
import domain.service.faultlogging.IProfile;

public class MockRepositoryFactory extends RepositoryFactory {


    @Override
    public Repository<RepairTeam> createRepairTeamRepository() {

        Repository<RepairTeam> repo = new MockRepository<RepairTeam>();

        return repo;

    }

    @Override
    public Repository<WorkOrder> createWorkOrderRepository() {
    	Repository<WorkOrder> repo = new MockRepository<WorkOrder>(); 

    	return repo;
    }

    @Override
    public Repository<Fault> createFaultRepository() {
        Repository<Fault> repo = new MockRepository<Fault>();
        return repo;
    }

    @Override
    protected Repository<Profile> createProfileRepository() {
        Repository<Profile> profileRepository = new MockRepository<Profile>();
        //Initialize few profiles
        profileRepository.add(new Profile(1, "callCenterAgentA", "password", IProfile.PROFILE_ROLE.CALL_CENTER_OPERATOR));
        profileRepository.add(new Profile(2, "faultInspectorA", "password", IProfile.PROFILE_ROLE.FAULT_INSPECTOR));
        profileRepository.add(new Profile(3, "faultInvestigatorA", "password", IProfile.PROFILE_ROLE.FAULT_INVESTIGATOR));
        profileRepository.add(new Profile(4, "faultSupervisortA", "password", IProfile.PROFILE_ROLE.FAULT_SUPERVISOR));
        profileRepository.add(new Profile(5, "repairTeamLeaderA", "password", IProfile.PROFILE_ROLE.REPAIR_TEAM_LEADER));
        profileRepository.add(new Profile(6, "systemUserA", "password", IProfile.PROFILE_ROLE.SYSTEM_USER));
        profileRepository.add(new Profile(7, "workOrderCreator", "password", IProfile.PROFILE_ROLE.WORK_ORDER_CREATOR));
        return profileRepository;
    }
}
