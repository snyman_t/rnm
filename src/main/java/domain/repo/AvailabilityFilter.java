package domain.repo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AvailabilityFilter {
	
	private String startDateTime;
	private int duration;
	
	public AvailabilityFilter(String startDateTime,int duration) throws ParseException{
		this.startDateTime = startDateTime;
		this.duration = duration;
	}
	
	public String getStartDateTime() {
		return startDateTime;
	}

	public int getDuration() {
		return duration;
	}

	
	
	
	
	
	
}
