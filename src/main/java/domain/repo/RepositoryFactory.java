package domain.repo;

import domain.model.faultrepair.Fault;
import domain.model.faultrepair.RepairTeam;
import domain.model.faultrepair.WorkOrder;
import domain.service.faultlogging.Profile;

public abstract class RepositoryFactory {


	// Singleton design pattern
	// To ensure that we only have to change the Repository implementation in 1 place and that's here. 
	public static RepositoryFactory getFactoryInstance(){
		RepositoryFactory instance = new MockRepositoryFactory();
		return instance;
	}
	
	public Repository<RepairTeam> getRepairTeamRepository(){
		Repository<RepairTeam> repo = createRepairTeamRepository();
		return repo;
	}
	
	public Repository<WorkOrder> getWorkOrderRepository(){
		Repository<WorkOrder> repo = createWorkOrderRepository();
		return repo;
	}
	
	public Repository<Fault> getFaultRepository(){
		Repository<Fault> repo = createFaultRepository();
		return repo;
	}
	
	public Repository<Profile> getProfileRepository(){
		Repository<Profile> repo = createProfileRepository();
		return repo;
	}
    // Below are the Factory methods
	// These methods defines how the repository gets created. For instance a MockRepository needs to be initialized. 
	// The initialization can be implemented in these methods 
	
	abstract Repository<RepairTeam> createRepairTeamRepository();
	abstract Repository<WorkOrder> createWorkOrderRepository();
	abstract Repository<Fault> createFaultRepository();
	protected abstract Repository<Profile> createProfileRepository();

}
