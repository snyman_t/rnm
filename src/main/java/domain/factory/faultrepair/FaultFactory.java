package domain.factory.faultrepair;

import domain.model.faultrepair.Fault;
import domain.model.faultrepair.RepairTeam;
import domain.model.faultrepair.WorkOrder;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;

public class FaultFactory {

	private Repository<Fault> repository;
	private Fault f;
	
	public FaultFactory(Repository<Fault> repository){
		this.repository = repository;
	}
	
	public Fault buildAggregate(int repairTeamId){
		f = this.repository.findById(repairTeamId);
		return f;
	}
}