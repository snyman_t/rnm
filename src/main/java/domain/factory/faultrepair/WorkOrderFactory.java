package domain.factory.faultrepair;

import domain.model.faultrepair.Fault;
import domain.model.faultrepair.WorkOrder;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;

public class WorkOrderFactory {

	private Repository<WorkOrder> repository;
	private WorkOrder wo;
	
	public WorkOrderFactory(Repository<WorkOrder> repository){
		this.repository = repository;
	}
	
	public WorkOrder buildAggregate(int workOrderId){
		wo = this.repository.findById(workOrderId);
		return wo;
	}
}