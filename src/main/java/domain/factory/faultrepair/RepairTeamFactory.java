package domain.factory.faultrepair;

import domain.model.faultrepair.Fault;
import domain.model.faultrepair.RepairTeam;
import domain.model.faultrepair.WorkOrder;
import domain.repo.Repository;
import domain.repo.RepositoryFactory;

public class RepairTeamFactory {

	private Repository<RepairTeam> repository;
	private RepairTeam rt;
	
	public RepairTeamFactory(Repository<RepairTeam> repository){
		this.repository = repository;
	}
	
	public RepairTeam buildAggregate(int repairTeamId){
		rt = this.repository.findById(repairTeamId);
		return rt;
	}
	

}