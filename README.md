# README #

Welcome to the RNM Project. 

### What is this repository for? ###

This is a java application for the group project in the Software Analysis and Design course (ELEN7045) at the University of the Witwatersrand
(The ELEN7045 subject is part of the M Eng programme in Software Engineering)


### How do I get set up? ###

The project makes use of Maven. You can use the command "mvn test" to run the tests developed.


### Who do I talk to? ###

The following team members are contributing to the project:
Thinus Snyman
Shameer Kika
Steve Cross